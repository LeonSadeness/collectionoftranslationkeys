﻿using CollectionOfTranslationKeys.Commands.Entities;
using System.Runtime.Serialization;

namespace CollectionOfTranslationKeys.Commands.Exception
{
    public class CommandNotFoundException : CommandException
    {
        public CommandNotFoundException(Command cmd) : base(cmd) { }

        public CommandNotFoundException(Command cmd, string message) : base(cmd, message) { }

        public CommandNotFoundException(Command cmd, string message, System.Exception inner) : base(cmd, message, inner) { }

        protected CommandNotFoundException(Command cmd, SerializationInfo info, StreamingContext context) : base(cmd, info, context) { }
    }
}
