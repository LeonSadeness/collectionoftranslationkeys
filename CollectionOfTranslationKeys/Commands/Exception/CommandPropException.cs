﻿using CollectionOfTranslationKeys.Commands.Entities;
using System.Runtime.Serialization;

namespace CollectionOfTranslationKeys.Commands.Exception
{
    class CommandPropException : CommandException
    {
        public CommandProp Prop { get; set; }
        public CommandPropException(Command cmd, CommandProp prop) : base(cmd) { Prop = prop; }

        public CommandPropException(Command cmd, CommandProp prop, string message) : base(cmd, message) { Prop = prop; }

        public CommandPropException(Command cmd, CommandProp prop, string message, System.Exception inner) : base(cmd, message, inner) { Prop = prop; }

        protected CommandPropException(Command cmd, CommandProp prop, SerializationInfo info, StreamingContext context) : base(cmd, info, context) { Prop = prop; }
    }
}
