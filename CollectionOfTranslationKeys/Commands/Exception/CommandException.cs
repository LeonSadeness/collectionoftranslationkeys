﻿using CollectionOfTranslationKeys.Commands.Entities;

namespace CollectionOfTranslationKeys.Commands.Exception
{
    public class CommandException : System.Exception
    {
        public Command Command { get; private set; }

        public CommandException(Command cmd) { Command = cmd; }

        public CommandException(Command cmd, string message) : base(message) { Command = cmd; }

        public CommandException(Command cmd, string message, System.Exception inner) : base(message, inner) { Command = cmd; }

        protected CommandException(
          Command cmd,
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { Command = cmd; }
    }
}
