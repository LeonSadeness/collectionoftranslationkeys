﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionOfTranslationKeys.Commands.Entities
{
    public class CommandContext
    {
        public object[] Params { get; set; }
    }
}
