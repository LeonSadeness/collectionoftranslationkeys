﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CollectionOfTranslationKeys.Commands.Entities
{
    public class CommandProp
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string[] Aliases { get; set; } = new string[0];

        public IEnumerable<string> AllNames => Aliases?.Append(Name) ?? new string[] { Name };

        public Type TypeProp { get; set; }

        public bool IsRequired { get; set; }

        public object DefaultValue { get; set; }
    }
}
