﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CollectionOfTranslationKeys.Commands.Entities
{
    public class Command
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string[] Aliases { get; set; } = new string[0];

        public IEnumerable<string> AllNames => Aliases?.Append(Name) ?? new string[] { Name };

        public CommandProp[] Props { get; set; }

        public Action<CommandContext> Action { get; set; }
    }
}
