﻿using CollectionOfTranslationKeys.Commands.Entities;
using CollectionOfTranslationKeys.Commands.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CMD = СollectionOfTranslationKeys.Utilities.CMDUtilities;

namespace CollectionOfTranslationKeys.Commands
{
    public class CommandManager
    {
        private Command _helpCommand;
        public Command HelpCommand
        {
            get
            {
                var result = _helpCommand ??= CommandsList.FirstOrDefault(i => i.Name == "help");
                if (result == null)
                {
                    result = CreateHelpCommand();
                    CommandsList = CommandsList.Append(result);
                    _helpCommand = result;
                }

                return _helpCommand;
            }
        }

        public IEnumerable<Command> CommandsList { get; private set; }

        public CommandManager(IEnumerable<Command> cmdList)
        {
            CommandsList = cmdList;
        }

        Command CreateHelpCommand()
        {
            var helpCommand = new Command
            {
                Name = "help",
                Description = "Вывод информации о командах",
                Props = new CommandProp[] {
                    new CommandProp
                    {
                        Name = "--command",
                        Aliases = new string[]{ "-c"},
                        Description = "Имя определенной команды. Пример (help -с parse)",
                        TypeProp = typeof(string),
                        IsRequired = false
                    }
                },
                Action = (ctx) =>
                {
                    if (ctx?.Params?.Count() > 0)
                    {
                        var cmdName = ctx.Params[0].ToString();
                        if (TryFindCommand(cmdName, out Command cmd))
                        {
                            Console.WriteLine();
                            WriteCmdDescription(cmd);
                        }
                        else
                            CMD.WriteLineWithColor($"Command \"{cmdName}\" not found", ConsoleColor.Red);
                    }
                    else
                    {
                        foreach (var cmd in CommandsList)
                        {
                            Console.WriteLine();
                            WriteCmdDescription(cmd);
                        }
                    }
                }
            };
            return helpCommand;
        }

        void WriteCmdDescription(Command cmd)
        {
            // Name
            Console.Write("Help info command ");
            CMD.WriteWithColor(cmd.Name, ConsoleColor.Red);
            if (cmd?.Aliases?.Count() > 0)
            {
                Console.Write(" (");
                for (int i = 0; i < cmd.Aliases.Length; i++)
                {
                    CMD.WriteWithColor(cmd.Aliases[i], ConsoleColor.Red);
                    if (i < cmd.Aliases.Length - 1)
                        CMD.WriteWithColor(", ", ConsoleColor.DarkGray);
                }
                Console.WriteLine(")");
            }
            else Console.WriteLine();

            // Description
            if (!string.IsNullOrWhiteSpace(cmd.Description))
                CMD.WriteLineWithColor(cmd.Description, ConsoleColor.DarkGray);

            // Prop
            if (cmd?.Props?.Count() > 0)
            {
                CMD.WriteLineWithColor($"   {new string('_', 16)}", ConsoleColor.DarkYellow);
                foreach (var prop in cmd.Props)
                {
                    Console.Write("    ");
                    CMD.WriteWithColor($"{prop.Name}", ConsoleColor.DarkYellow);
                    if(prop?.Aliases?.Count() > 0)
                    {
                        Console.Write(" (");
                        for (int i = 0; i < prop.Aliases.Length; i++)
                        {
                            CMD.WriteWithColor(prop.Aliases[i], ConsoleColor.DarkYellow);
                            if (i < prop.Aliases.Length - 1)
                                CMD.WriteWithColor(", ", ConsoleColor.DarkGray);
                        }
                        Console.Write(")");
                    }
                    CMD.WriteLineWithColor($" - {prop.Description}", ConsoleColor.DarkGray);
                }
            }
        }

        public bool TryFindCommand(string cmdName, out Command cmd)
        {
            if (cmdName == "help")
            {
                cmd = HelpCommand;
                return true;
            }
            cmd = CommandsList.FirstOrDefault(i => i.AllNames.Contains(cmdName));
            return cmd != null;
        }

        public CommandContext ParseProps(Command cmd, string[] args)
        {
            var result = new CommandContext();

            if (args != null && cmd.Props != null)
            {
                //var unknownArg = args.FirstOrDefault(i => cmd.Props.All(j => !j.AllNames.Contains(i)));
                //if (unknownArg != null)
                //    throw new CommandException(cmd, $"Unknown argument \"{unknownArg}\" comand \"{cmd.Name}\'");

                List<object> parametrs = new List<object>();

                for (int i = 0; i < cmd.Props.Length; i++)
                {
                    var prop = cmd.Props[i];

                    int argIndex = Array.FindIndex(args, a => prop.AllNames.Contains(a));

                    if (prop.TypeProp == typeof(bool))
                    {
                        parametrs.Add(argIndex > -1);
                    }
                    else
                    {
                        if (prop.IsRequired && argIndex == -1) throw new CommandPropException(cmd, prop, $"Prop \"{prop.Name}\" is required");

                        if (argIndex > -1)
                            switch (prop.TypeProp.Name.ToLower())
                            {
                                case "string":
                                    if (args.Length - 1 < argIndex + 1)
                                        throw new CommandPropException(cmd, prop, $"Prop \"{prop.Name}\" require string value");
                                    else
                                    {
                                        parametrs.Add(args[argIndex + 1]);
                                        i++;
                                    }
                                    break;
                                default:
                                    throw new NotImplementedException("Parse value of prop is not implemented");
                            }
                    }
                    if (parametrs.Count > 0) result.Params = parametrs.ToArray();
                }
            }
            return result;
        }

        public bool TryRunCommand(string[] args, ref string msg)
        {
            if (args != null && args.Length > 0 && TryFindCommand(args[0], out Command cmd))
            {
                try
                {
                    var paramArgs = args[1..];
                    var ctx = ParseProps(cmd, paramArgs);
                    RunCommand(cmd, ctx);
                    return true;
                }
                catch (CommandException ex)
                {
                    msg = ex.Message;
                    return false;
                }
            }
            msg = $"Command \"{args[0]}\" not found";
            return false;
        }

        public void RunCommand(Command cmd, CommandContext ctx)
        {
            cmd.Action(ctx);
        }
    }
}