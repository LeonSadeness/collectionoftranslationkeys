﻿using CollectionOfTranslationKeys.Commands;
using CollectionOfTranslationKeys.Commands.Entities;
using COTK_Utilities.Enums;
using COTK_Utilities.Services.Config;
using COTK_Utilities.Services.Parser;
using COTK_Utilities.Services.Parser.Entities;
using COTK_Utilities.Services.Updater;
using COTK_Utilities.Services.Updater.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using CMD = СollectionOfTranslationKeys.Utilities.CMDUtilities;

namespace СollectionOfTranslationKeys
{
    class Program
    {
        private static readonly string _author = "Leon Sadeness";
        private static readonly string _nameProgram = "Сollection Of Translation Keys";
        private static readonly string _version = "3.5.2";
        private static Configuration _config;
        private static Parser _parser;

        private static char _charL = '┗';
        private static char _charE = '┣';
        private static char _char_ = '━';

        #region Commands

        private static readonly CommandProp _nochkProp = new CommandProp
        {
            Name = "--nocheckconfig",
            Aliases = new string[] { "-nochk" },
            IsRequired = false,
            TypeProp = typeof(bool),
        };
        private static readonly CommandProp _detailProp = new CommandProp
        {
            Name = "--detail",
            Aliases = new string[] { "-info", "-i" },
            Description = "Вывод подробной информации результата",
            IsRequired = false,
            TypeProp = typeof(bool),
        };
        private static readonly CommandProp _writeProp = new CommandProp
        {
            Name = "--write",
            Aliases = new string[] { "-w" },
            Description = "Предложить записать результат",
            IsRequired = false,
            TypeProp = typeof(bool),
        };
        private static readonly CommandProp _forceProp = new CommandProp
        {
            Name = "--force",
            Aliases = new string[] { "-f" },
            Description = "Форсированное действие записи без подтверждения",
            IsRequired = false,
            TypeProp = typeof(bool),
        };

        private static readonly IEnumerable<Command> _commandsList = new List<Command>
        {
            new Command
            {
                Name = "parse",
                Description = "Парсит исходные файлы на наличие ключей перевода",
                Props = new CommandProp[] { _writeProp, _forceProp },
                Action = (ctx) => StartParse((bool)ctx.Params[0], (bool)ctx.Params[1]),
            },
            new Command
            {
                Name = "update",
                Description = "Обновляет найденные ключи рабочих словарей",
                Props = new CommandProp[] {
                    new CommandProp
                    {
                        Name = "--translate",
                        Aliases = new string[] { "-t" },
                        IsRequired = false,
                        TypeProp = typeof(bool),
                    },
                    _detailProp,
                    _writeProp,
                    _forceProp,
                },
                Action = (ctx) => StartUpdate((bool)ctx.Params[0], (bool)ctx.Params[1], (bool)ctx.Params[2],  (bool)ctx.Params[3]),
            },
            new Command
            {
                Name = "missing",
                Description = "Ищет непереведенные ключи в рабочих словарях и записывает в хранилище",
                Aliases = new string[]{ "miss"},
                Props = new CommandProp[] {
                    _detailProp,
                    _writeProp,
                    _forceProp,
                },
                Action = (ctx) => StartMakeMissing((bool)ctx.Params[0], (bool)ctx.Params[1], (bool)ctx.Params[2]),
            },
            new Command
            {
                Name = "purge",
                Description = "Чистит хранилище переводов, и обьединяет их в один файл",
                Props = new CommandProp[] {
                    _detailProp,
                    _writeProp,
                    _forceProp,
                },
                Action = (ctx) => StartPurgeStores((bool)ctx.Params[0], (bool)ctx.Params[1], (bool)ctx.Params[2]),
            },
        };
        private static readonly CommandManager _commandManager = new CommandManager(_commandsList);

        #endregion

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            if (args.Length > 0)
            {
                if (!CheckConfig(true)) return;
                string msg = string.Empty;
                bool isSuccess = _commandManager.TryRunCommand(args, ref msg);
                if (!isSuccess) CMD.WriteLineWithColor(msg, ConsoleColor.Red);
            }
            else
                RunUI();
        }

        static void RunUI()
        {
            // SplashScreen
            WriteSplashScreen();

            // Config
            var existConfig = CheckConfig();
            if (!existConfig)
            {
                Console.WriteLine("Press any key for exit");
                Console.ReadKey(true);
                return;
            }

            // UI
            string[] exitCmds = { "exit", "q", "quit", "escape", "close", "shutdown" };
            string line;
            do
            {
                line = Console.ReadLine().Trim();
                if (exitCmds.Contains(line)) break;

                var args = line.Split(' ');
                string msg = string.Empty;
                bool isSuccess = _commandManager.TryRunCommand(args, ref msg);
                if (!isSuccess) CMD.WriteLineWithColor(msg, ConsoleColor.Red);
            }
            while (!exitCmds.Contains(line));

            Console.WriteLine($"{_nameProgram} is shutdown.");
            return;
        }

        #region Common

        /// <summary>
        /// Показать заставку с информацией о приложении
        /// </summary>
        static void WriteSplashScreen()
        {
            var mainTextA = $" {_nameProgram}";
            var mainTextB = $" {_version} ";
            var authorText = $"by {_author}";
            var lengthText = new string[]
            {
                $"{mainTextA}{mainTextB}",
                authorText
            }.Max(i => i.Length);

            var tl = '╭';
            var tr = '╮';
            var br = '╯';
            var bl = '╰';
            var horizontal = '─';
            var vertical = '│';
            var ml = ' ';

            var line1 = $"{ml}{tl}{new string(horizontal, lengthText)}{tr}";
            var line2a = $"{ml}{vertical}";
            var line2b = mainTextA;
            var line2c = mainTextB;
            var line2d = $"{vertical}";
            var line3 = $"{ml}{vertical} {new string('─', lengthText - 2)} {vertical}";
            var line4a = $"{ml}{vertical}";
            var line4b = $" {new string(' ', lengthText - authorText.Length - 2)}{authorText} ";
            var line4c = $"{vertical}";
            var line5 = $"{ml}{bl}{new string('─', lengthText)}{br}";

            var borderColor = ConsoleColor.DarkGray;
            var titleColor = ConsoleColor.DarkCyan;
            var versionColor = ConsoleColor.Cyan;
            var footerColor = ConsoleColor.DarkGray;

            CMD.WriteLineWithColor(line1, borderColor);

            CMD.WriteWithColor(line2a, borderColor);
            CMD.WriteWithColor(line2b, titleColor);
            CMD.WriteWithColor(line2c, versionColor);
            CMD.WriteLineWithColor(line2d, borderColor);

            CMD.WriteLineWithColor(line3, borderColor);

            CMD.WriteWithColor(line4a, borderColor);
            CMD.WriteWithColor(line4b, footerColor);
            CMD.WriteLineWithColor(line4c, borderColor);

            CMD.WriteLineWithColor(line5, borderColor);
        }

        /// <summary>
        /// Проверяем наличие конфигурационнго файла и предлагаем создать, если нету.
        /// </summary>
        /// <returns>Наличие настроенной конфигурации</returns>
        static bool CheckConfig(bool isForce = false)
        {
            Console.WriteLine("Check configuration...");

            _config = new Configuration();
            bool isNoConfig = !_config.TryReadConfig();

            if (isNoConfig)
            {
                bool isCreateEmpty = false;
                if (!isForce)
                    isCreateEmpty = CMD.ConfirmMessage($"There is no configuration.\nWould you like to create a configuration skeleton in\n{_config.ConfigPath}?");
                if (isCreateEmpty)
                {
                    Configuration.WriteEmptyConfig(Configuration.DefaultConfigPath);
                    Console.Write("Configuration skeleton is created. Check the file ");
                    CMD.WriteLineWithColor($"\"{Configuration.DefaultNameConfig}\"", ConsoleColor.Yellow);

                }
                else
                {
                    CMD.WriteLineWithColor("There is no configuration. Work stopped.", ConsoleColor.Red);
                    Console.WriteLine();
                }
                return false;
            }

            CMD.WriteLineWithColor("Сonfiguration found", ConsoleColor.Green);
            Console.WriteLine();
            return true;
        }

        static void WriteUpdateResult(UpdateResult updateResult, bool isForce)
        {
            if (!isForce)
            {
                bool isConfirmed = CMD.ConfirmMessage($"\nDo you want to write result?");
                if (!isConfirmed)
                {
                    CMD.WriteLineWithColor($"You have rejected the write of the result\n", ConsoleColor.Red);
                    return;
                }
            }

            Console.WriteLine("Write update results...");

            updateResult.ProgressEvent += (float progress) => CMD.ProgressBar(progress, "Write");
            updateResult.WriteResult().Wait();

            Console.Write("Write is ");
            CMD.WriteWithColor($"Done\n", ConsoleColor.Green);
        }

        #endregion

        #region Parse

        static void StartParse(bool isWrite = false, bool isForce = false)
        {
            Console.WriteLine("Start parse...");

            // Start
            var stopWatchParse = new Stopwatch();
            stopWatchParse.Start();

            _parser = new Parser(_config.ServiceConfig);
            _parser.ProgressEvent += (float progress) => CMD.ProgressBar(progress, "Collect");
            _parser.Start();

            // Stop
            stopWatchParse.Stop();

            var count = _parser.Result.AllCollections?.Count() ?? 0;
            Console.WriteLine("time: {0}", stopWatchParse.Elapsed.ToString(@"mm\:ss\.ff"));
            Console.Write("Generate ");
            CMD.WriteLineWithColor($"{count} files. ", ConsoleColor.Yellow);

            // Write
            if (isWrite)
            {
                if (isForce)
                {
                    WriteParseResult(_parser.Result);
                }
                else
                {
                    bool isWriteParse = CMD.ConfirmMessage($"Do you want to write the result to the folder  \"{_config.ServiceConfig.ParserConfig.EmptyKeyGenerationDir}\"");
                    if (isWriteParse) WriteParseResult(_parser.Result);
                }
            }

            Console.WriteLine();
        }

        static void WriteParseResult(ParseResult result)
        {
            Console.WriteLine("Write parse results...");

            result.WriteProgressEvent += (float progress) => CMD.ProgressBar(progress);
            result.WriteResult(_config.ServiceConfig);

            Console.Write("\nWrite is ");
            CMD.WriteWithColor($"Done", ConsoleColor.Green);
            Console.WriteLine();
        }
        #endregion

        #region Update

        static void StartUpdate(bool isTranslate, bool isDetail, bool isWrite, bool isForce)
        {
            if (isTranslate)
                StartUpdateTranslate(isDetail, isWrite, isForce);
            else
                StartUpdateKeys(isDetail, isWrite, isForce);
        }

        static void StartUpdateKeys(bool isDetail, bool isWrite, bool isForce)
        {
            // Parse
            if (_parser == null) StartParse();

            // Update
            var updateResult = CheckUpdateKeys(isDetail);

            // Create
            int keyUpdateCount = updateResult.GetCountKeys();
            bool isUpdatable = keyUpdateCount > 0;
            var color = isUpdatable ? ConsoleColor.DarkYellow : ConsoleColor.Green;
            var text = isUpdatable
                ? $"{keyUpdateCount} changes can be applied"
                : "Nothing to update";
            CMD.WriteLineWithColor(text, color);
            Console.WriteLine();

            // Write
            if (isUpdatable && isWrite) WriteUpdateResult(updateResult, isForce);
        }

        static void StartUpdateTranslate(bool isDetail, bool isWrite, bool isForce)
        {
            // Update
            var updateResult = CheckUpdateTranslate(isDetail);

            // Create
            int keyUpdateCount = updateResult.GetCountKeys();
            bool isUpdatable = keyUpdateCount > 0;
            var color = isUpdatable ? ConsoleColor.DarkYellow : ConsoleColor.Green;
            var text = isUpdatable
                ? $"{keyUpdateCount} changes can be applied"
                : "Nothing to update";
            CMD.WriteLineWithColor(text, color);
            Console.WriteLine();

            // Write
            if (isUpdatable && isWrite) WriteUpdateResult(updateResult, isForce);
        }

        static UpdateResult CheckUpdateKeys(bool isDetailInfo)
        {
            var trimCount = 10;

            Console.WriteLine("Start check update keys...");

            var updater = new Updater(_config.ServiceConfig);
            updater.ProgressEvent += (float progress) => CMD.ProgressBar(progress, "Check update");
            UpdateResult resultUpdate = updater.UpdateAllKeys(_parser.Result);

            Console.WriteLine("End check update keys...");

            foreach (var pack in resultUpdate.Changes)
            {
                Console.WriteLine();
                Console.Write(" Pack ");
                CMD.WriteWithColor($"({pack.UpdateDictPaths.PagesDir})", ConsoleColor.Yellow);
                Console.Write(" && ");
                CMD.WriteLineWithColor($"({pack.UpdateDictPaths.CommonFilePath})", ConsoleColor.Yellow);

                foreach (var book in pack.WordbookList)
                {
                    Console.WriteLine();
                    CMD.WriteWithColor($"  [{pack.UpdateDictPaths.Locale}] ", ConsoleColor.Yellow);
                    CMD.WriteWithColor($"[{book.ChangeType}] ", GetConsoleColor(book.ChangeType));
                    CMD.WriteLineWithColor(book.Name + ".json", ConsoleColor.DarkYellow);
                    CMD.WriteLineWithColor($"  {new string(_char_, 32)}", ConsoleColor.DarkGray);

                    var pairList = book.OnlyChangeDict
                        .OrderBy(i => i.Key)
                        .ToList();
                    var isShortList = !isDetailInfo && pairList.Count() > trimCount;
                    var countView = isShortList ? trimCount : pairList.Count();

                    for (int j = 0; j < countView; j++)
                    {
                        var pair = pairList.ElementAt(j);

                        // ┗ or ┣
                        var prefixKey = j == pairList.Count() - 1 ? _charL : _charE;
                        CMD.WriteWithColor($"      {prefixKey} ", ConsoleColor.DarkGray);

                        CMD.WriteWithColor($"[{pair.Modification.ToString().Substring(0, 3)}] ", GetConsoleColor(pair.Modification));
                        Console.WriteLine(pair.Key);
                    }
                    if (isShortList)
                    {
                        CMD.WriteWithColor($"      {_charL} ", ConsoleColor.DarkGray);
                        Console.Write("... ");
                        CMD.WriteLineWithColor($"and {pairList.Count() - trimCount} more", GetConsoleColor(book.ChangeType));
                    }
                }
            }
            Console.WriteLine();

            return resultUpdate;
        }

        static UpdateResult CheckUpdateTranslate(bool isDetailInfo)
        {
            var trimCount = 10;

            Console.WriteLine("Start check update translate...");

            var updater = new Updater(_config.ServiceConfig);
            updater.ProgressEvent += (float progress) => CMD.ProgressBar(progress, "Check update");

            UpdateResult resultUpdate = updater.UpdateAllValues();
            Console.WriteLine("End check update translate...");

            foreach (var pack in resultUpdate.Changes)
            {
                Console.WriteLine();
                CMD.WriteWithColor($" ({pack.UpdateDictPaths.PagesDir})", ConsoleColor.Yellow);
                Console.Write(" && ");
                CMD.WriteLineWithColor($"({pack.UpdateDictPaths.CommonFilePath})", ConsoleColor.Yellow);

                foreach (var book in pack.WordbookList)
                {
                    Console.WriteLine();
                    CMD.WriteWithColor($"  [{pack.UpdateDictPaths.Locale}] ", ConsoleColor.Yellow);
                    CMD.WriteLineWithColor($"{book.Name}.json", ConsoleColor.DarkYellow);
                    CMD.WriteLineWithColor($"  {new string(_char_, 32)}", ConsoleColor.DarkGray);

                    var pairList = book.OnlyChangeDict
                        .OrderBy(i => i.Key)
                        .ToList();
                    var isShortList = !isDetailInfo && pairList.Count() > trimCount;
                    var countView = isShortList ? trimCount : pairList.Count();

                    for (int j = 0; j < countView; j++)
                    {
                        var pair = pairList.ElementAt(j);

                        // ┣ or ┗
                        var prefixKey = j == pairList.Count() - 1 ? _charL : _charE;
                        CMD.WriteWithColor($"      {prefixKey} ", ConsoleColor.DarkGray);

                        CMD.WriteWithColor($"[{pair.Modification.ToString().Substring(0, 3)}] ", GetConsoleColor(pair.Modification));
                        CMD.WriteWithColor($"{pair.Key}: ", ConsoleColor.Gray);
                        if (pair.Modification == ModificationType.UPDATED)
                        {
                            CMD.WriteWithColor($"\"{book.Dict[pair.Key]}\"", ConsoleColor.DarkGray);
                            CMD.WriteWithColor(" to ", GetConsoleColor(pair.Modification));
                        }
                        CMD.WriteLineWithColor($"\"{pair.Value}\"", ConsoleColor.Gray);
                    }
                    if (isShortList)
                    {
                        CMD.WriteWithColor($"      {_charL} ", ConsoleColor.DarkGray);
                        Console.Write("... ");
                        CMD.WriteLineWithColor($"and {pairList.Count() - trimCount} more", GetConsoleColor(book.ChangeType));
                    }
                }
            }
            Console.WriteLine();

            return resultUpdate;
        }

        #endregion

        #region Missing

        private static void StartMakeMissing(bool isDetail, bool isWrite = false, bool isForce = false)
        {
            // Update
            var updateResult = MakeMissing(isDetail);

            // Create
            int keyUpdateCount = updateResult.GetCountKeys();
            bool isUpdatable = keyUpdateCount > 0;
            var color = isUpdatable ? ConsoleColor.DarkYellow : ConsoleColor.Green;
            var text = isUpdatable
                ? $"Missing {keyUpdateCount} keys"
                : "All translated";
            CMD.WriteLineWithColor(text, color);
            Console.WriteLine();

            // Write
            if (isUpdatable && isWrite) WriteUpdateResult(updateResult, isForce);
        }

        private static MissingResult MakeMissing(bool isDetailInfo)
        {
            var trimCount = 10;

            Console.WriteLine("Start check missing translate...");

            var updater = new Updater(_config.ServiceConfig);
            updater.ProgressEvent += (float progress) => CMD.ProgressBar(progress, "Check missing");

            MissingResult resultUpdate = updater.CreateAllMissingTranslate();
            Console.WriteLine("End check missing translate...");

            foreach (var pack in resultUpdate.Changes)
            {
                Console.WriteLine();
                CMD.WriteWithColor($" [{pack.UpdateDictPaths.Locale}]", ConsoleColor.DarkYellow);
                Console.WriteLine(" missing translate:");
                CMD.WriteLineWithColor($"  \"{pack.WordbookList[0].Name}.json)", ConsoleColor.DarkGray);
                CMD.WriteLineWithColor($"   {new string(_char_, 32)}", ConsoleColor.DarkYellow);

                var pairList = pack.WordbookList[0].AllDict.OrderBy(i => i.Key).AsEnumerable();
                var isShortList = !isDetailInfo && pairList.Count() > trimCount;
                var countView = isShortList ? trimCount : pairList.Count();

                for (int i = 0; i < countView; i++)
                {
                    var pair = pairList.ElementAt(i);

                    var count = pack.WordbookList[0].AllDict.Count() - 1;
                    var prefixKey = i == count ? _charL : _charE;
                    CMD.WriteWithColor($"     {prefixKey} ", ConsoleColor.DarkYellow);

                    Console.WriteLine(pair.Key);
                }
                if (isShortList)
                {
                    CMD.WriteWithColor($"     {_charL} ", ConsoleColor.DarkYellow);
                    Console.Write("... ");
                    CMD.WriteLineWithColor($"and {pairList.Count() - trimCount} more", ConsoleColor.DarkYellow);
                }
            }
            Console.WriteLine();

            return resultUpdate;
        }
        #endregion

        #region Store

        private static void StartPurgeStores(bool isDetail, bool isWrite = false, bool isForce = false)
        {
            // Update
            var updateResult = PurgeStores(isDetail);

            // Create
            int keyUpdateCount = updateResult.GetCountKeys();
            bool isUpdatable = keyUpdateCount > 0;
            var color = isUpdatable ? ConsoleColor.DarkYellow : ConsoleColor.Green;
            var text = isUpdatable
                ? $"Can merge {keyUpdateCount} keys"
                : "All clear";
            CMD.WriteLineWithColor(text, color);
            Console.WriteLine();

            // Write
            if (isUpdatable && isWrite) WriteUpdateResult(updateResult, isForce);
        }

        private static PurgeStoreResult PurgeStores(bool isDetailInfo)
        {
            var trimCount = 10;

            Console.WriteLine("Start check purge store...");

            var updater = new Updater(_config.ServiceConfig);
            updater.ProgressEvent += (float progress) => CMD.ProgressBar(progress, "Check clear");

            PurgeStoreResult resultUpdate = updater.PurgeAllStores();
            Console.WriteLine("End check purhe store...");

            foreach (var pack in resultUpdate.Changes)
            {

                Console.WriteLine();
                CMD.WriteWithColor($" [{pack.UpdateDictPaths.Locale}]", ConsoleColor.DarkYellow);
                Console.WriteLine(" store:");

                // Added wordbook
                var addedBook = pack.WordbookList.FirstOrDefault(i => i.ChangeType == ModificationType.ADDED);

                Console.WriteLine();
                CMD.WriteWithColor($"  [{pack.UpdateDictPaths.Locale}] ", ConsoleColor.Yellow);
                CMD.WriteWithColor($"[ADDED] ", ConsoleColor.Green);
                CMD.WriteLineWithColor(addedBook.Name + ".json", ConsoleColor.DarkYellow);

                var pairList = addedBook.OnlyChangeDict
                    .Where(i => i.ContainTag("dupl"))
                    .OrderBy(i => i.Key)
                    .ToList();
                var isShortAddList = !isDetailInfo && pairList.Count() > trimCount;
                var countViewAddList = isShortAddList ? trimCount : pairList.Count();

                CMD.WriteWithColor($"  Combined ", ConsoleColor.Green);
                Console.Write(addedBook.AllDict.Count());
                CMD.WriteLineWithColor(" keys", ConsoleColor.Green);
                CMD.WriteWithColor($"  Removed ", ConsoleColor.Yellow);
                Console.Write(pairList.Count());
                CMD.WriteLineWithColor(" duplicates:", ConsoleColor.Yellow);

                CMD.WriteLineWithColor($"  {new string(_char_, 32)}", ConsoleColor.DarkGray);

                for (int j = 0; j < countViewAddList; j++)
                {
                    var pair = pairList.ElementAt(j);

                    // ┗ or ┣
                    var prefixKey = j == pairList.Count() - 1 ? _charL : _charE;
                    CMD.WriteWithColor($"    {prefixKey} ", ConsoleColor.DarkGray);

                    CMD.WriteWithColor($"[DUPL] ", ConsoleColor.Yellow);
                    Console.WriteLine(pair.Key);
                }
                if (isShortAddList)
                {
                    CMD.WriteWithColor($"    {_charL} ", ConsoleColor.DarkGray);
                    Console.Write("... ");
                    CMD.WriteLineWithColor($"and {pairList.Count() - trimCount} more", ConsoleColor.Yellow);
                }

                // Deleted wordbooks
                var deletedBooks = pack.WordbookList
                    .Where(i => i.ChangeType == ModificationType.DELETED)
                    .OrderBy(i => i.Name)
                    .ToList();
                var isShortDelList = !isDetailInfo && deletedBooks.Count() > trimCount;
                var countViewDelList = isShortAddList ? trimCount : deletedBooks.Count();

                Console.WriteLine();
                CMD.WriteWithColor($"  [{pack.UpdateDictPaths.Locale}] ", ConsoleColor.Yellow);
                CMD.WriteWithColor($"[DELETED] ", ConsoleColor.Red);
                CMD.WriteWithColor($"{deletedBooks.Count()} ", ConsoleColor.Gray);
                CMD.WriteLineWithColor("files:", ConsoleColor.DarkYellow);
                CMD.WriteLineWithColor($"  {new string(_char_, 32)}", ConsoleColor.DarkGray);

                for (int i = 0; i < countViewDelList; i++)
                {
                    var book = deletedBooks[i];

                    // ┗ or ┣
                    var prefixKey = i == deletedBooks.Count() - 1 ? _charL : _charE;
                    CMD.WriteWithColor($"     {prefixKey} ", ConsoleColor.DarkGray);

                    CMD.WriteWithColor($"[DEL] ", ConsoleColor.Red);
                    Console.WriteLine($"{book.Name}.json");

                }
                if (isShortAddList)
                {
                    CMD.WriteWithColor($"     {_charL} ", ConsoleColor.DarkGray);
                    Console.Write("... ");
                    CMD.WriteLineWithColor($"and {deletedBooks.Count() - trimCount} more", ConsoleColor.Red);
                }
            }
            Console.WriteLine();

            return resultUpdate;
        }

        #endregion

        #region Utilities

        //static void MergeKeysAsValueWithKeys()
        //{
        //    var isForce = true;

        //    var dirKeys = Path.Join(Directory.GetCurrentDirectory(), "tt", "en");
        //    var dirValues = Path.Join(Directory.GetCurrentDirectory(), "tt", "es");
        //    var dirResult = Path.Join(Directory.GetCurrentDirectory(), "tt", "result");
        //    Directory.CreateDirectory(dirResult);

        //    var filesKeys = Directory.GetFiles(dirKeys, "*.json");
        //    var filesValues = Directory.GetFiles(dirValues, "*.json");

        //    foreach (var path in filesKeys)
        //    {
        //        var nameFileKeys = Path.GetFileNameWithoutExtension(path);
        //        var jsonKeys = File.ReadAllText(path);
        //        var dictKeys = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonKeys);

        //        var findedFileValues = filesValues.FirstOrDefault(i => Path.GetFileNameWithoutExtension(i) == nameFileKeys);
        //        if (findedFileValues == null)
        //        {
        //            Console.Write("NOT finded is: ");
        //            CMD.WriteLineWithColor(nameFileKeys, ConsoleColor.Red);
        //            Console.WriteLine();
        //            Console.ReadKey(true);
        //            continue;
        //        }

        //        var jsonValues = File.ReadAllText(findedFileValues);
        //        var dictValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonValues);
        //        if (dictValues.Count != dictKeys.Count)
        //        {
        //            Console.WriteLine("NOT Equals length: ");
        //            CMD.WriteWithColor(nameFileKeys, ConsoleColor.Red);
        //            Console.Write(" Values - ");
        //            CMD.WriteWithColor(dictValues.Count.ToString(), ConsoleColor.Red);
        //            Console.Write(" Keys - ");
        //            CMD.WriteLineWithColor(dictKeys.Count.ToString(), ConsoleColor.Green);
        //            Console.WriteLine();
        //            Console.ReadKey(true);
        //            continue;
        //        }

        //        var resultDict = new JObject();
        //        for (int i = 0; i < dictKeys.Count; i++)
        //        {
        //            var pair = dictKeys.ElementAt(i);
        //            resultDict[pair.Key] = dictValues.ElementAt(i).Key;
        //        }

        //        var resultFilePath = Path.Join(dirResult, $"{nameFileKeys}.json");

        //        if (isForce || CMD.ConfirmMessage($"Is write - {nameFileKeys}"))
        //        {
        //            using StreamWriter sw = new StreamWriter(resultFilePath, false);
        //            sw.WriteAsync(resultDict.ToString()).Wait();
        //            Console.Write(nameFileKeys);
        //            CMD.WriteLineWithColor(" - writed\n", ConsoleColor.Green);
        //            Console.WriteLine();
        //        }
        //        else
        //        {
        //            Console.Write(nameFileKeys);
        //            CMD.WriteLineWithColor(" - skiped\n", ConsoleColor.Red);
        //            Console.WriteLine();
        //        }
        //        if (!isForce) Console.ReadKey(true);
        //    }
        //}

        static private ConsoleColor GetConsoleColor(ModificationType type)
        {
            return type switch
            {
                ModificationType.NOTHING => ConsoleColor.DarkGray,
                ModificationType.DELETED => ConsoleColor.Red,
                ModificationType.ADDED => ConsoleColor.Green,
                ModificationType.UPDATED => ConsoleColor.DarkCyan,
                _ => ConsoleColor.Gray,
            };
        }

        #endregion
    }
}
