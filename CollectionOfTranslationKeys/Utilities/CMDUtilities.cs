﻿using System;
using System.Collections.ObjectModel;

namespace СollectionOfTranslationKeys.Utilities
{
    class CMDUtilities
    {
        public static bool ConfirmMessage(string msg)
        {
            Console.Write($"{msg} ");
            WriteLineWithColor("<Y/N>", ConsoleColor.Green);
            ConsoleKey key;
            do
            {
                key = Console.ReadKey(true).Key;
                if (key == ConsoleKey.Y) return true;
            } while (key != ConsoleKey.N && key != ConsoleKey.Escape);
            return false;
        }

        public static void ProgressBar(float progress, string title = "Progress")
        {
            if (progress <= 1)
            {
                if (progress > 0)
                {
                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                }
                var percent = Math.Round(progress * 100, 2);
                int countFull = (int)(percent * 0.25);
                int countEmpty = (int)(25 - countFull);
                var bar = $"{new string('█', countFull)}{new string('-', countEmpty)}";
                var offset = new string(' ', 5 - percent.ToString().Length);
                Console.Write("{0} {1}%{2} ", title, percent, offset);

                WriteLineWithColor(bar, ConsoleColor.Green);
            }
        }

        public static void WriteLineWithColor(string msg, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(msg);
            Console.ResetColor();
        }

        public static void WriteWithColor(string msg, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.Write(msg);
            Console.ResetColor();
        }
    }
}
