dotnet publish -r win-x64 -c Release -p:PublishSingleFile=true --self-contained false

dotnet build --configuration Release --runtime ubuntu.16.04-x64

dotnet publish --configuration Release --self-contained true --runtime linux-x64 --verbosity quiet -p:PublishSingleFile=true

<PublishSingleFile>true</PublishSingleFile>