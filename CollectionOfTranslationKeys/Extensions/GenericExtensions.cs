﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CollectionOfTranslationKeys.Extensions
{
    public static class GenericExtensions
    {
        public static bool ContainsAny<T>(this IEnumerable<T> expected, params T[] arr)
        {
            return expected.ContainsAny(arr);
        }

        public static bool ContainsAny<T>(this IEnumerable<T> expected, IEnumerable<T> arr)
        {
            return expected.Any(i => arr.Contains(i));
        }
    }
}
