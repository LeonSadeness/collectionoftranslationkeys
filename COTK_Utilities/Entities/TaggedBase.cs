﻿using COTK_Utilities.Interfaces;
using System.Linq;

namespace COTK_Utilities.Entities
{
    public abstract class TaggedBase : ITagged
    {
        private ITag[] _tags;        
        /// <summary>
        /// Коллекция тегов
        /// </summary>
        public ITag[] Tags
        {
            get => _tags ??= new Tag[0];
            set => _tags = value;
        }

        /// <summary>
        /// Проверяет наличие тэга
        /// </summary>
        /// <param name="tagName">Имя тэга</param>
        /// <returns>Булево значение наличие тэга</returns>
        public bool ContainTag(string tagName)
        {
            return Tags.Any(i => i.Name.ToLower() == tagName.ToLower());
        }

        /// <summary>
        /// Устанавливает тег, и возвращает true если он не был установлен ранее, и false если такой тэг уже был
        /// </summary>
        /// <param name="name">Имя тэга</param>
        /// <param name="description">Описание тэга</param>
        /// <returns>Наличие такого тэга</returns>
        public bool SetTag(string name, string description = null)
        {
            if(!ContainTag(name))
            {
                Tags = Tags.Append(new Tag { Name = name, Description = description }).ToArray();
                return true;
            }
            return false;
        }
    }
}
