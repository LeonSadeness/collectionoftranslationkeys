﻿using COTK_Utilities.Interfaces;
using COTK_Utilities.Services.Config.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace COTK_Utilities.Entities
{
    /// <summary>
    /// Локализированное хранилище переводов
    /// </summary>
    public class LocalePackStore : TaggedBase, ILocalePackStore
    {
        /// <summary>
        /// Абсолютный путь к дирректории
        /// </summary>
        public string Dir { get; set; }

        /// <summary>
        /// Имя дирректории
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Локаль переводов
        /// </summary>
        public string Locale { get; set; }

        /// <summary>
        /// Коллекция файлов хранящих переведенные ключи
        /// </summary>
        public IWordbook[] StoreCollections { get; set; }

        #region Constructor

        public LocalePackStore(LocalePackPaths packPaths, ITag tag) : this(packPaths, new ITag[1] { tag }) { }

        public LocalePackStore(LocalePackPaths packPaths, ITag[] tags = null)
        {
            Dir = packPaths.TranslatedKeyStoreDir;
            StoreCollections = GetStoreCollectionFromDir();
            Locale = packPaths.Locale;
            Tags = tags;
        }

        public LocalePackStore(string locale, string dir, ITag tag) : this(locale, dir, new ITag[1] { tag }) { }

        public LocalePackStore(string locale, string dir, ITag[] tags = null)
        {
            Dir = dir;
            StoreCollections = GetStoreCollectionFromDir();
            Locale = locale;
            Tags = tags;
        }
        #endregion

        /// <summary>
        /// Возвращает все коллекции хранилица которые находятся в инициализированной директории
        /// </summary>
        /// <returns>Коллекции хранилища</returns>
        IWordbook[] GetStoreCollectionFromDir()
        {
            if (Directory.Exists(Dir))
            {
                List<Wordbook> result = new List<Wordbook>();
                var files = Directory.GetFiles(Dir, "*.json");

                foreach (var path in files)
                {
                    var json = File.ReadAllText(path);
                    var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                    if (dict != null)
                    {
                        var store = new Wordbook
                        {
                            Dict = dict,
                            Path = path,
                        };
                        result.Add(store);
                    }
                }

                if (result.Count > 0) return result.ToArray();
            }
            return new Wordbook[0];
        }

        /// <summary>
        /// Получить все ключи со всех хранилищ обьединенных в словаре
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, string> GetAllCollections()
        {
            var result = StoreCollections
                .SelectMany(i => i.Dict, (s, d) => d)
                .Where(i => !string.IsNullOrWhiteSpace(i.Value))
                .Aggregate(new Dictionary<string, string>(), (a, c) =>
                {
                    a[c.Key] = c.Value;
                    return a;
                });

            return result;
        }
    }
}
