﻿using COTK_Utilities.Enums;
using COTK_Utilities.Interfaces;
using System.Collections.Generic;

namespace COTK_Utilities.Entities
{
using System.Linq;
    public class PairInfo : TaggedBase, IPairInfo
    {
        /// <summary>
        /// Ключ пары
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Значение пары
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Тип информации
        /// </summary>
        public ModificationType Modification { get; set; }

        #region Constructors

        public PairInfo() { }

        public PairInfo(string key, string value)
        {
            Key = key;
            Value = value;
        }

        #endregion

        public static implicit operator KeyValuePair<string, string>(PairInfo pairInfo) 
            => new KeyValuePair<string, string>(pairInfo.Key, pairInfo.Value);

        public static implicit operator PairInfo(KeyValuePair<string, string> pair)
            => new PairInfo(pair.Key, pair.Value);

        public override bool Equals(object obj)
        {
            if (obj is null)
                throw new System.ArgumentNullException(nameof(obj));
            if (obj is PairInfo pair) return Key == pair.Key;
            return false;
        }
        public override int GetHashCode() => Key.GetHashCode();
    }
}
