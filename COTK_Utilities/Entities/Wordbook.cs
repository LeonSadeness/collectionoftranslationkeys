﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using COTK_Utilities.Interfaces;

namespace COTK_Utilities.Entities
{
    /// <summary>
    /// Именованная коллекция ключей
    /// </summary>
    public class Wordbook : FileInformation, IWordbook
    {
        private string _name;
        /// <summary>
        /// Имя словаря, используется в качестве имени файла
        /// </summary>
        public new string Name
        {
            get => _name ?? System.IO.Path.GetFileNameWithoutExtension(Path ?? "");
            set => _name = value;
        }

        /// <summary>
        /// Коллекция ключей
        /// </summary>
        public IDictionary<string, string> Dict { get; set; } = new ConcurrentDictionary<string, string>();
    }
}
