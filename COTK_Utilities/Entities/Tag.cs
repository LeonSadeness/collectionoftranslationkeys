﻿using COTK_Utilities.Interfaces;

namespace COTK_Utilities.Entities
{
    /// <summary>
    /// Тэговая метка для группировки сущностей
    /// </summary>
    public class Tag : ITag
    {
        /// <summary>
        /// Имя тэга
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание тэга
        /// </summary>
        public string Description { get; set; }
    }
}
