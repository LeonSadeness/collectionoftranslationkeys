﻿using COTK_Utilities.Interfaces;

namespace COTK_Utilities.Entities
{
    /// <summary>
    /// Информация о файле
    /// </summary>
    public class FileInformation : TaggedBase, IFileInfo
    {
        /// <summary>
        /// Абсолютный путь к файлу
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Имя файла
        /// </summary>
        public string Name { get; set; }
    }
}
