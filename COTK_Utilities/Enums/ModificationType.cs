﻿namespace COTK_Utilities.Enums
{
    public enum ModificationType
    {
        /// <summary>
        /// Без изменений
        /// </summary>
        NOTHING = 0,
        /// <summary>
        /// Пара удалена
        /// </summary>
        DELETED = 1,
        /// <summary>
        /// Пара добавлена
        /// </summary>
        ADDED = 2,
        /// <summary>
        /// К ключу добавлено значение
        /// </summary>
        UPDATED = 3,
    }
}
