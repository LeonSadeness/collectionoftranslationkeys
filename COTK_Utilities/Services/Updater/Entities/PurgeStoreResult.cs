﻿using COTK_Utilities.Enums;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COTK_Utilities.Services.Updater.Entities
{
    public class PurgeStoreResult : UpdateResult
    {
        public override int GetCountKeys()
        {
            int keyUpdateCount = Changes.Aggregate(
                    0,
                    (accPack, colPack) => accPack + colPack.WordbookList.Aggregate(
                        0,
                        (accColl, colColl) => accColl + colColl.OnlyChangeDict.Count()
                    )
                );

            return keyUpdateCount;
        }

        public override async Task WriteResult()
        {
            int currentKeys = 0;

            if (TotalKeys == 0) return;

            foreach (var pack in Changes)
            {

                // Delete
                var deleteBooks = pack.WordbookList.Where(i => i.ChangeType == ModificationType.DELETED).ToList();

                foreach (var book in deleteBooks)
                {
                    if (File.Exists(book.Path)) File.Delete(book.Path);

                    currentKeys += book.OnlyChangeDict.Count();
                    float progress = (float)(currentKeys) / (float)(TotalKeys);
                    ChangeProgress(progress);
                }

                // Add
                var addedBook = pack.WordbookList.FirstOrDefault(i => i.ChangeType == ModificationType.ADDED);
                var addedDict = addedBook.AllDict
                    .OrderBy(i => i.Key)
                    .Select(i => new KeyValuePair<string, string>(i.Key, i.Value));

                var o = new JObject();
                foreach (var pair in addedDict)
                {
                    o[pair.Key] = pair.Value;

                    float progress = (float)++currentKeys / (float)TotalKeys;
                    ChangeProgress(progress);
                }
                using (StreamWriter sw = new StreamWriter(addedBook.Path, false))
                {
                    await sw.WriteAsync(o.ToString());
                }
            }

            await Task.CompletedTask;
        }
    }
}
