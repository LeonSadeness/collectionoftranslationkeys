﻿using System.Collections.Generic;
using COTK_Utilities.Services.Config.Entities;

namespace COTK_Utilities.Services.Updater.Entities
{
    /// <summary>
    /// Обновления словаря с файлами ключей
    /// </summary>
    public class LocalePackUpdated
    {
        /// <summary>
        /// Пути обновляемых словарей
        /// </summary>
        public LocalePackPaths UpdateDictPaths { get; set; }

        /// <summary>
        /// Обновленные коллекции ключей и их изменения
        /// </summary>
        public List<WordbookUpdated> WordbookList { get; set; }
    }
}
