﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace COTK_Utilities.Services.Updater.Entities
{
    public class MissingResult : UpdateResult
    {
        public override int GetCountKeys()
        {
            int keyUpdateCount = Changes.Aggregate(
                    0,
                    (accPack, colPack) => accPack + colPack.WordbookList.Aggregate(
                        0,
                        (accColl, colColl) => accColl + colColl.OnlyChangeDict.Count()
                    )
                );

            return keyUpdateCount;
        }

        public override async Task WriteResult()
        {
            int currentKeys = 0;

            if (TotalKeys == 0) return;

            foreach (var pack in Changes)
            {
                Directory.CreateDirectory(Path.Join(pack.UpdateDictPaths.TranslatedKeyStoreDir));

                var wordbook = pack.WordbookList[0];
                var dictUpdated = wordbook.AllDict.Select(i => new KeyValuePair<string, string>(i.Key, i.Value));
                var dictUnion = wordbook.Dict.Union(dictUpdated).OrderBy(i => i.Key);
                var o = new JObject();

                foreach (var pair in dictUnion)
                {
                    o[pair.Key] = pair.Value;

                    float progress = (float)++currentKeys / (float)TotalKeys;
                    ChangeProgress(progress);
                }
                using StreamWriter sw = new StreamWriter(wordbook.Path, false);
                await sw.WriteAsync(o.ToString());
            }

            await Task.CompletedTask;
        }
    }
}
