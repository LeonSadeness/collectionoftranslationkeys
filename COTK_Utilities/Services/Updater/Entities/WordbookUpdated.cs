﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using COTK_Utilities.Entities;
using COTK_Utilities.Enums;
using COTK_Utilities.Interfaces;

namespace COTK_Utilities.Services.Updater.Entities
{
    /// <summary>
    /// Обновления коллекции ключей
    /// </summary>
    public class WordbookUpdated : Wordbook
    {

        /// <summary>
        /// Тип изменения коллекции
        /// </summary>
        public ModificationType ChangeType { get; set; }

        /// <summary>
        /// Все пары (измененые и без изменений)
        /// </summary>
        public List<IPairInfo> AllDict { get; set; }

        /// <summary>
        /// Только измененные пары
        /// </summary>
        public List<IPairInfo> OnlyChangeDict => AllDict.Where(i => i.Modification != ModificationType.NOTHING).ToList();

        /// <summary>
        /// Возвращает пару соответствующую ключу из обновленного словаря или null если она не найдена
        /// </summary>
        /// <param name="key">Ключ пары</param>
        /// <returns>Пара из обновленного словаря</returns>
        public IPairInfo this[string key] => AllDict?.FirstOrDefault(i => i.Key == key);
    }
}
