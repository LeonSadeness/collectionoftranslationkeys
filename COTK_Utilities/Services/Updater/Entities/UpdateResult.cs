﻿using COTK_Utilities.Enums;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace COTK_Utilities.Services.Updater.Entities
{
    /// <summary>
    /// Результат обновления словарей
    /// </summary>
    public class UpdateResult
    {
        /// <summary>
        /// Всего обновленных ключей
        /// </summary>
        public int TotalKeys => GetCountKeys();

        /// <summary>
        /// Прогресс действия
        /// </summary>
        public event Action<float> ProgressEvent;

        /// <summary>
        /// Изменения словарей
        /// </summary>
        public List<LocalePackUpdated> Changes { get; set; } = new List<LocalePackUpdated>();

        public UpdateResult()
        {
            Changes = new List<LocalePackUpdated>();
        }

        public virtual int GetCountKeys()
        {
            int keyUpdateCount = Changes.Aggregate(
                    0,
                    (accPack, colPack) => accPack + colPack.WordbookList.Aggregate(
                        0,
                        (accColl, colColl) => accColl + colColl.OnlyChangeDict.Count()
                    )
                );

            return keyUpdateCount;
        }

        /// <summary>
        /// Записать результат
        /// </summary>
        public virtual async Task WriteResult()
        {
            int currentKeys = 0;

            if (TotalKeys == 0) return;

            foreach (var pack in Changes)
            {
                foreach (var wordbook in pack.WordbookList.Where(i => i.ChangeType != ModificationType.DELETED))
                {
                    var dict = wordbook.AllDict
                        .Where(i => i.Modification != ModificationType.DELETED)
                        .OrderBy(i => i.Key);
                    var o = new JObject();

                    foreach (var pair in dict)
                    {
                        o[pair.Key] = pair.Value;

                        float progress = (float)++currentKeys / (float)TotalKeys;
                        ChangeProgress(progress);
                    }

                    using StreamWriter sw = new StreamWriter(wordbook.Path, false);
                    await sw.WriteAsync(o.ToString());
                }
            }

            await Task.CompletedTask;
        }

        /// <summary>
        /// Обрабатывает смену прогресса
        /// </summary>
        /// <param name="progress">Текущий прогресс от 0 до 1</param>
        protected void ChangeProgress(float progress) => ProgressEvent?.Invoke(progress);
    }
}
