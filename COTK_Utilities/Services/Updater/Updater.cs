﻿using COTK_Utilities.Enums;
using COTK_Utilities.Entities;
using COTK_Utilities.Interfaces;
using COTK_Utilities.Services.Config.Entities;
using COTK_Utilities.Services.Extensions;
using COTK_Utilities.Services.Updater.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace COTK_Utilities.Services.Updater
{
    public partial class Updater
    {
        public readonly ServiceConfiguration Config;

        public delegate void ProgressEventHandler(float progress);
        public event ProgressEventHandler ProgressEvent;

        #region Constructor

        public Updater(ServiceConfiguration config)
        {
            Config = config;
        }
        #endregion

        /// <summary>
        /// Проверяет наличие и доступность проведения обновления локалей опираясь на конфигурацию
        /// <param name="msg">Текст сообщения содержащий подробности результата</param>
        /// </summary>
        /// <returns> Булево значение возможности обновление локали </returns>
        public bool CheckConfig(out string msg)
        {
            var updateLocales = Config?.UpdaterConfig?.UpdateDirs?.Where(d => d.ExistFiles(Config.RootDir));
            bool existFilesForUpdate = updateLocales?.Count() > 0;

            msg = existFilesForUpdate
                ? updateLocales.Aggregate("Updated files found for locales:", (a, c) => $"{a}\n    {c}")
                : "Update files not found.";

            return existFilesForUpdate;
        }

        /// <summary>
        /// Считывает и десереализует файл json с обьектом без вложенностей
        /// </summary>
        /// <param name="path">Абсолютный путь к файлу</param>
        /// <returns>Словарь ключей и значений json файла или null</returns>
        public Dictionary<string, string> ReadJson(string path)
        {
            if (!File.Exists(path)) return null;

            var json = File.ReadAllText(path);
            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            return result;
        }

        /// <summary>
        /// Считывает и десереализует файл json с обьектом без вложенностей
        /// </summary>
        /// <param name="path">Абсолютный путь к файлу</param>
        /// <returns>Словарь ключей и значений json файла или null</returns>
        public async Task<Dictionary<string, string>> ReadJsonAsync(string path)
        {
            if (!File.Exists(path)) return null;

            var json = await File.ReadAllTextAsync(path);
            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            return result;
        }
    }
}
