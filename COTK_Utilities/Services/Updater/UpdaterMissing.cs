﻿using COTK_Utilities.Entities;
using COTK_Utilities.Enums;
using COTK_Utilities.Interfaces;
using COTK_Utilities.Services.Config.Entities;
using COTK_Utilities.Services.Extensions;
using COTK_Utilities.Services.Updater.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace COTK_Utilities.Services.Updater
{
    public partial class Updater
    {
        /// <summary>
        /// Создает коллекции непереведенных ключей
        /// </summary>
        /// <returns>Результат проверки непереведенных ключей</returns>
        public MissingResult CreateAllMissingTranslate() => CreateAllMissingTranslate(Config);

        /// <summary>
        /// Создает коллекции непереведенных ключей
        /// </summary>
        /// <param name="config">Конфигурация сервиса</param>
        /// <returns>Результат проверки непереведенных ключей</returns>
        public MissingResult CreateAllMissingTranslate(ServiceConfiguration config)
        {
            MissingResult result = new MissingResult();
            var stores = config.UpdaterConfig.UpdateDirs.Where(i => !string.IsNullOrWhiteSpace(i?.TranslatedKeyStoreDir));

            int count = stores?.Count() ?? default;
            if (count == default) return null;

            for (int i = 0; i < stores.Count(); i++)
            {
                var store = stores.ElementAt(i);

                var findedDirForUpdate = config.UpdaterConfig.UpdateDirs.FirstOrDefault(u => u.Locale == store.Locale);
                if (findedDirForUpdate != null)
                {
                    var updated = CreateMissingTranslatePack(findedDirForUpdate, config);
                    if (updated != null)
                        result.Changes.Add(updated);
                }
            }

            return result;
        }

        /// <summary>
        /// Создает пакет локали с непереведенными ключами
        /// </summary>
        /// <param name="pack">Пути пакета локалей</param>
        /// <param name="config">Конфигурация сервиса</param>
        /// <returns>Пакет локали с непереведенными ключами</returns>
        public LocalePackUpdated CreateMissingTranslatePack(LocalePackPaths pack, ServiceConfiguration config)
        {
            var storeDir = Path.Join(config.RootDir, pack.TranslatedKeyStoreDir);
            var storeName = $"{pack.Locale}_missing_{DateTime.Now:dd.MM.yyyy}.json";
            var storePath = Path.Join(storeDir, storeName);

            LocalePackUpdated result = new LocalePackUpdated
            {
                UpdateDictPaths = pack,
                WordbookList = new List<WordbookUpdated>
                {
                   new WordbookUpdated
                   {
                       ChangeType = ModificationType.ADDED,
                       Dict = new Dictionary<string, string>(),
                       AllDict = new List<IPairInfo>(),
                       Path = storePath,
                   }
                },
            };


            List<string> paths = new List<string>();

            var commonFilePath = IOExtensions.GetDirInfo(pack.CommonFilePath, config.RootDir).FullName;
            if (File.Exists(commonFilePath))
                paths.Add(commonFilePath);

            var pageFilesDir = IOExtensions.GetDirInfo(pack.PagesDir, config.RootDir).FullName;
            if (Directory.Exists(pageFilesDir))
                paths.AddRange(Directory.GetFiles(pageFilesDir, "*.json"));

            foreach (var path in paths)
            {
                var dict = ReadJson(path);
                if (dict == null) continue;

                var missing = dict
                    .Where(i => string.IsNullOrWhiteSpace(i.Value))
                    .Select(i => new PairInfo(i.Key, i.Value) { Modification = ModificationType.ADDED });
                if (missing.Count() > 0)
                {
                    result.WordbookList[0].AllDict = result.WordbookList[0].AllDict.Union(missing).ToList();
                }
            }

            return result.WordbookList[0].AllDict.Count() > 0 ? result : null;
        }
    }
}
