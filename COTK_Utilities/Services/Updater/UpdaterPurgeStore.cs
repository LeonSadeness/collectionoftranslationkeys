﻿using COTK_Utilities.Entities;
using COTK_Utilities.Enums;
using COTK_Utilities.Interfaces;
using COTK_Utilities.Services.Config.Entities;
using COTK_Utilities.Services.Extensions;
using COTK_Utilities.Services.Updater.Entities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace COTK_Utilities.Services.Updater
{
    public partial class Updater
    {
        public PurgeStoreResult PurgeAllStores() => PurgeAllStores(Config);

        public PurgeStoreResult PurgeAllStores(ServiceConfiguration config)
        {
            var stores = config.UpdaterConfig.UpdateDirs
               .Where(i => !string.IsNullOrWhiteSpace(i?.TranslatedKeyStoreDir))
               .Select(i =>
               {
                   var path = Path.Join(config.RootDir, i.TranslatedKeyStoreDir);
                   var store = new LocalePackStore(i.Locale, path);
                   return store;
               }).ToArray();

            var result = new PurgeStoreResult
            {
                Changes = new List<LocalePackUpdated>()
            };

            foreach (var store in stores)
            {
                var updated = PurgeStore(store);
                if (updated != null)
                    result.Changes.Add(updated);
            }

            return result;
        }

        public LocalePackUpdated PurgeStore(LocalePackStore pack)
        {
            var files = new string[0];

            if (Directory.Exists(pack.Dir))
            {
                files = Directory.GetFiles(pack.Dir, "*.json");
                if (files.Length == 0) return null;
            }
            else return null;

            var deletedBooks = new BlockingCollection<WordbookUpdated>();
            var mainBookPairs = new BlockingCollection<PairInfo>();
            Parallel.ForEach(files, (file) => PurgeWordbook(ref mainBookPairs, ref deletedBooks, file));

            var result = new LocalePackUpdated
            {
                UpdateDictPaths = new LocalePackPaths
                {
                    Locale = pack.Locale,
                    TranslatedKeyStoreDir = pack.Dir
                },
                WordbookList = deletedBooks.ToList()
            };
            var mainBook = new WordbookUpdated
            {
                ChangeType = ModificationType.ADDED,
                Path = Path.Join(pack.Dir, $"{pack.Locale}_store_{DateTime.Now:dd.MM.yyyy}.json"),
                AllDict = DistinctPairInfoWhithTag(mainBookPairs.ToList<IPairInfo>())
            };

            if (mainBook.AllDict.Count > 0)
                result.WordbookList = result.WordbookList.Prepend(mainBook).ToList();

            return result;
        }

        void PurgeWordbook(ref BlockingCollection<PairInfo> mainBookPairs, ref BlockingCollection<WordbookUpdated> deletedBooks, string file)
        {
            var store = ReadJson(file);
            if (store != null)
            {
                var updatedStore = new WordbookUpdated
                {
                    Dict = store,
                    Path = file,
                    ChangeType = ModificationType.DELETED,
                    AllDict = new List<IPairInfo>()
                };

                foreach (var pair in store)
                {
                    var deletePair = new PairInfo(pair.Key, pair.Value) { Modification = ModificationType.DELETED };
                    updatedStore.AllDict.Add(deletePair);

                    if (!string.IsNullOrWhiteSpace(pair.Value))
                    {
                        var addedPair = new PairInfo(pair.Key, pair.Value) { Modification = ModificationType.ADDED };
                        mainBookPairs.Add(addedPair);
                    }
                }

                deletedBooks.Add(updatedStore);
            }
        }

        List<IPairInfo> DistinctPairInfoWhithTag(ICollection<IPairInfo> list, string tagDuplicate = "dupl")
        {
            var result = new List<IPairInfo>();

            foreach (var pair in list)
            {
                var finded = result.Find(i => i.Key == pair.Key);
                if (finded != null)
                {
                    finded.SetTag(tagDuplicate, "Duplicate");
                }
                else
                {
                    result.Add(pair);
                }
            }

            return result;
        }
    }
}
