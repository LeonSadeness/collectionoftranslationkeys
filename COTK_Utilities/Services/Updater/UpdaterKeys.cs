﻿using COTK_Utilities.Entities;
using COTK_Utilities.Enums;
using COTK_Utilities.Interfaces;
using COTK_Utilities.Services.Config.Entities;
using COTK_Utilities.Services.Extensions;
using COTK_Utilities.Services.Parser.Entities;
using COTK_Utilities.Services.Updater.Entities;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace COTK_Utilities.Services.Updater
{
    public partial class Updater
    {
        /// <summary>
        /// Обновляет ключи во всех паках локалей на диске на основе результата парсинга и возвращает результат обновления
        /// </summary>
        /// <param name="parseResult">Результат парсинга</param>
        /// <returns>Результат обновления</returns>
        public UpdateResult UpdateAllKeys(ParseResult parseResult) => UpdateAllKeys(parseResult, Config);

        /// <summary>
        /// Обновляет ключи во всех паках локалей на диске на основе результата парсинга и возвращает результат обновления
        /// </summary>
        /// <param name="parseResult">Результат парсинга</param>
        /// <param name="config">Конфигурация</param>
        /// <returns>Результат обновления</returns>
        public UpdateResult UpdateAllKeys(ParseResult parseResult, ServiceConfiguration config)
        {
            UpdateResult result = new UpdateResult();

            var count = config?.UpdaterConfig?.UpdateDirs?.Length ?? default;
            if (count > 0)
            {
                for (int i = 0; i < config.UpdaterConfig.UpdateDirs.Length; i++)
                {
                    var pack = config.UpdaterConfig.UpdateDirs[i];

                    var updated = UpdateKeysPack(pack, parseResult);
                    if (updated != null) result.Changes.Add(updated);

                    ProgressEvent?.Invoke((float)(i + 1) / (float)count);
                }
            }

            return result;
        }

        /// <summary>
        /// Возвращает обновленные коллекции с изменениями для конкрктного пака локали
        /// </summary>
        /// <param name="pack">Пак локали для обновления</param>
        /// <param name="parseResult">Результаты парсинга</param>
        /// <returns>Обновленные данные ключей для пака локали</returns>
        public LocalePackUpdated UpdateKeysPack(LocalePackPaths pack, ParseResult parseResult)
        {
            LocalePackUpdated result = new LocalePackUpdated
            {
                UpdateDictPaths = pack,
                WordbookList = new List<WordbookUpdated>()
            };

            #region COM
            string commonFilePath = IOExtensions.GetDirInfo(pack.CommonFilePath, Config.RootDir).FullName;
            var comUpdated = UpdateKeysWordbook(commonFilePath, parseResult.CommonKeys);
            if (comUpdated != null)
                result.WordbookList.Add(comUpdated);
            #endregion

            #region PAGES
            string pagesDir = IOExtensions.GetDirInfo(pack.PagesDir, Config.RootDir).FullName;
            Directory.CreateDirectory(pagesDir);
            string[] pagePaths = Directory.GetFiles(pagesDir, "*.json", SearchOption.AllDirectories);

            foreach (var parsePageCollection in parseResult.PagesKeys)
            {
                var fileToUpdatePath = pagePaths.FirstOrDefault(i => Path.GetFileNameWithoutExtension(i) == parsePageCollection.Name)
                    ?? Path.Join(pagesDir, parsePageCollection.Name + ".json");
                var updated = UpdateKeysWordbook(fileToUpdatePath, parsePageCollection);
                if (updated != null)
                    result.WordbookList.Add(updated);
            }

            var deleteCollPaths = pagePaths.Where(i =>
            {
                var fileName = Path.GetFileNameWithoutExtension(i);
                var isNotExist = parseResult.PagesKeys.All(p => p.Name != fileName);
                return isNotExist;
            });
            foreach (var path in deleteCollPaths)
            {
                var keys = ReadJson(path) ?? new Dictionary<string, string>();
                var delWordbook = new WordbookUpdated
                {
                    ChangeType = ModificationType.DELETED,
                    Path = path,
                    AllDict = keys.Select(i => new PairInfo
                    {
                        Key = i.Key,
                        Value = i.Value,
                        Modification = ModificationType.DELETED,
                    } as IPairInfo).ToList()
                };
                result.WordbookList.Add(delWordbook);
            }
            #endregion

            return result.WordbookList.Count > 0 ? result : null;
        }

        /// <summary>
        /// Проверяет и записывает обновленную коллекцию ключей на основе старого файла
        /// </summary>
        /// <param name="oldFilePath">Путь к старому файлу</param>
        /// <param name="parsedResultKeys">Информация о новой спарсенной коллекции ключей</param>
        /// <returns>Обновленная версия ключей, с их изменениями</returns>
        public WordbookUpdated UpdateKeysWordbook(string oldFilePath, IWordbook parsedResultKeys)
        {
            bool isExistFile = File.Exists(oldFilePath);
            var oldDict = ReadJson(oldFilePath) ?? new Dictionary<string, string>();

            var result = UpdateKeysWordbook(oldDict, parsedResultKeys.Dict);
            if (result?.AllDict != null)
            {
                result.ChangeType = isExistFile ? ModificationType.UPDATED : ModificationType.ADDED;
                result.Name = Path.GetFileNameWithoutExtension(oldFilePath);
                result.Path = oldFilePath;
            }

            return result;
        }

        /// <summary>
        /// Проверяет и записывает обновленную коллекцию ключей на основе старой колллекци
        /// </summary>
        /// <param name="oldDict">Старая коллекция ключей</param>
        /// <param name="newDict">Новая коллекция ключей</param>
        /// <returns>Обновленная версия ключей, с их изменениями</returns>
        public WordbookUpdated UpdateKeysWordbook(IDictionary<string, string> oldDict, IDictionary<string, string> newDict)
        {
            var addedList = newDict.Where(i => oldDict.All(j => j.Key != i.Key));
            var removeList = oldDict.Where(i => newDict.All(j => j.Key != i.Key));
            var noChangeList = oldDict.Where(i => newDict.Any(j => j.Key == i.Key));

            WordbookUpdated result = null;

            if (removeList.Count() > 0 || addedList.Count() > 0)
            {
                var addedInfo = addedList.Select(i => new PairInfo(i.Key, i.Value) { Modification = ModificationType.ADDED });
                var deletedInfo = removeList.Select(i => new PairInfo(i.Key, i.Value) { Modification = ModificationType.DELETED });
                var noChangeInfo = noChangeList.Select(i => new PairInfo(i.Key, i.Value) { Modification = ModificationType.NOTHING });

                var allChanges = new List<IPairInfo>(addedInfo.Concat(deletedInfo).Concat(noChangeInfo).OrderBy(i => i.Key));

                result = new WordbookUpdated
                {
                    Dict = oldDict,
                    AllDict = allChanges,
                };
            }

            return result;
        }
    }
}
