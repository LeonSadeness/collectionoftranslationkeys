﻿using COTK_Utilities.Entities;
using COTK_Utilities.Enums;
using COTK_Utilities.Interfaces;
using COTK_Utilities.Services.Config.Entities;
using COTK_Utilities.Services.Extensions;
using COTK_Utilities.Services.Updater.Entities;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace COTK_Utilities.Services.Updater
{
    public partial class Updater
    {
        /// <summary>
        /// Обновляет переводы для всех паков локалей, используя хранилища переводов указанные в текущем конфиге
        /// </summary>
        /// <returns>Результат обновления</returns>
        public UpdateResult UpdateAllValues() => UpdateAllValues(Config);

        /// <summary>
        /// Обновляет переводы для всех паков локалей, используя хранилища переводов указанные в конфиге
        /// <param name="config">Конфигурация сервисов, содержащая папки для обновления с хранилищами ключей</paramref>
        /// </summary>
        /// <returns>Результат обновления</returns>
        public UpdateResult UpdateAllValues(ServiceConfiguration config)
        {
            var stores = config.UpdaterConfig.UpdateDirs
                .Where(i => !string.IsNullOrWhiteSpace(i?.TranslatedKeyStoreDir))
                .Select(i =>
                {
                    var path = Path.Join(config.RootDir, i.TranslatedKeyStoreDir);
                    var packPaths = new LocalePackPaths
                    {
                        CommonFilePath = i.CommonFilePath,
                        Locale = i.Locale,
                        PagesDir = i.PagesDir,
                        TranslatedKeyStoreDir = path
                    };
                    var store = new LocalePackStore(packPaths);
                    return store;
                }).ToArray();
            return UpdateAllValues(config, stores);
        }

        /// <summary>
        /// Обновляет переводы для всех паков локалей, используя хранилища переводов
        /// </summary>
        /// <param name="config">Конфигурация сервисов, содержащая папки для обновления с хранилищами ключей</paramref>
        /// <param name="stores">Коллекция локализированных хранилиц с переводами</param>
        /// <returns>Результат обновления</returns>
        public UpdateResult UpdateAllValues(ServiceConfiguration config, ILocalePackStore[] stores)
        {
            UpdateResult result = new UpdateResult();

            int count = stores.Length;
            if (count == default) return null;

            for (int i = 0; i < stores.Length; i++)
            {
                var store = stores[i];

                var findedDirForUpdate = config.UpdaterConfig.UpdateDirs.FirstOrDefault(u => u.Locale == store.Locale);
                if (findedDirForUpdate != null)
                {
                    var updated = UpdateValuesPack(config, findedDirForUpdate, store);
                    if (updated != null)
                        result.Changes.Add(updated);
                }
            }

            return result;
        }

        /// <summary>
        /// Ищет и добавляет перевод для ключей
        /// </summary>
        /// <param name="config">Конфигурация сервисов, содержащая папки для обновления с хранилищами ключей</paramref>
        /// <param name="pack">Пак локали для обновления переводов</param>
        /// <param name="store">Коллекция ключей и значений для добавления перевода</param>
        /// <returns>Обновленные данные переводов для пака локали</returns>
        public LocalePackUpdated UpdateValuesPack(ServiceConfiguration config, LocalePackPaths pack, ILocalePackStore store)
        {
            LocalePackUpdated result = new LocalePackUpdated
            {
                UpdateDictPaths = pack,
                WordbookList = new List<WordbookUpdated>(),
            };

            var storeDict = store.GetAllCollections();

            var commonFilePath = IOExtensions.GetDirInfo(pack.CommonFilePath, config.RootDir).FullName;
            if (File.Exists(commonFilePath))
            {
                var updated = UpdateValuesCollection(commonFilePath, storeDict);
                if (updated != null)
                {
                    updated.ChangeType = ModificationType.UPDATED;
                    result.WordbookList.Add(updated);
                }
            }

            var pageDir = IOExtensions.GetDirInfo(pack.PagesDir, Config.RootDir).FullName;
            if (Directory.Exists(pageDir))
            {
                var pagePaths = Directory.GetFiles(pageDir, "*.json");
                foreach (var path in pagePaths)
                {
                    var updated = UpdateValuesCollection(path, storeDict);
                    if (updated != null)
                        result.WordbookList.Add(updated);
                }
            }

            return result.WordbookList.Count > 0 ? result : null;
        }

        /// <summary>
        /// Обновляет или добавляет перевод из хранилища для коллекции
        /// </summary>
        /// <param name="pathCollection">Путь к файлу с ключами для обновления перевода</param>
        /// <param name="storeDict">Хранилище переводов</param>
        /// <returns>Результат обновленных переводов с изменениями</returns>
        public WordbookUpdated UpdateValuesCollection(string pathCollection, IDictionary<string, string> storeDict)
        {
            var collectionDict = ReadJson(pathCollection);

            var result = UpdateValuesCollection(collectionDict, storeDict);
            if (result != null)
            {
                result.ChangeType = ModificationType.UPDATED;
                result.Path = pathCollection;
            }

            return result;
        }

        /// <summary>
        /// Обновляет или добавляет перевод из хранилища для коллекции
        /// </summary>
        /// <param name="collection">Коллекция со значениями</param>
        /// <param name="store">Хранилище переводов</param>
        /// <returns>Результат обновленных переводов с изменениями</returns>
        public WordbookUpdated UpdateValuesCollection(IDictionary<string, string> collection, IDictionary<string, string> store)
        {
            if (store == null) return null;

            WordbookUpdated result = new WordbookUpdated
            {
                Dict = collection,
                AllDict = new List<IPairInfo>(),
            };

            foreach (var item in collection)
            {
                var change = new PairInfo
                {
                    Key = item.Key,
                    Value = item.Value,
                    Modification = ModificationType.NOTHING
                };

                if (store.ContainsKey(item.Key) && collection[item.Key] != store[item.Key])
                {
                    bool isAdded = string.IsNullOrWhiteSpace(item.Value);
                    var text = isAdded
                        ? "Added translation of the key from the vault"
                        : "Updated translation of the key from the vault";
                    change.Value = store[item.Key];
                    change.Description = text;
                    change.Modification = isAdded ? ModificationType.ADDED : ModificationType.UPDATED;
                }
                result.AllDict.Add(change);
            }

            return result.OnlyChangeDict.Count() > 0 ? result : null;
        }
    }
}
