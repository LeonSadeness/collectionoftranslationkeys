﻿using Newtonsoft.Json;
using System;
using System.IO;
using COTK_Utilities.Services.Config.Entities;

namespace COTK_Utilities.Services.Config
{
    [JsonObject]
    public class Configuration
    {
        #region Static Props

        [JsonIgnore]
        public static string DefaultNameConfig => "config-cotk.json";
        [JsonIgnore]
        public static string DefaultConfigDir => Directory.GetCurrentDirectory();
        [JsonIgnore]
        public static string DefaultConfigPath => Path.Join(DefaultConfigDir, DefaultNameConfig);

        #endregion

        #region General Props
        private static string _configDir;
        [JsonIgnore]
        public string ConfigDir => _configDir ?? DefaultConfigDir;

        private string _nameConfig;
        [JsonIgnore]
        public string NameConfig => _nameConfig ?? DefaultNameConfig;

        [JsonIgnore]
        public string ConfigPath => Path.Join(ConfigDir, NameConfig);

        #endregion

        public event ErrorEventHandler ErrorEvent;

        public ServiceConfiguration ServiceConfig { get; set; } = new ServiceConfiguration();

        #region Constructor

        public Configuration(string configDir = null, string fullNameConfig = null)
        {
            _configDir = configDir;
            _nameConfig = fullNameConfig;
        }
        #endregion

        public bool TryReadConfig()
        {
            if (!File.Exists(ConfigPath)) return false;

            try
            {
                using StreamReader reader = File.OpenText(ConfigPath);
                string json = reader.ReadToEnd();
                var settings = new JsonSerializerSettings
                {
                    StringEscapeHandling = StringEscapeHandling.Default,
                };
                var jConfig = JsonConvert.DeserializeObject<ServiceConfiguration>(json, settings);
                ServiceConfig = jConfig;

                return true;
            }
            catch (Exception ex)
            {
                ErrorEvent?.Invoke(this, new ErrorEventArgs(ex));
                return false;
            }
        }

        #region Static Methods

        public static async void WriteEmptyConfig(string path)
        {
            JsonSerializerSettings jsonSettings = new JsonSerializerSettings
            {
                DefaultValueHandling = DefaultValueHandling.Include,
                CheckAdditionalContent = true,
                Formatting = Formatting.Indented
            };

            var empty = new ServiceConfiguration();
            var json = JsonConvert.SerializeObject(empty, jsonSettings);

            using StreamWriter sw = new StreamWriter(path, false);
            await sw.WriteAsync(json);
        }

        #endregion
    }
}
