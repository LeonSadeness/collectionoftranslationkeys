﻿using Newtonsoft.Json;
using System.ComponentModel;
using COTK_Utilities.Interfaces;

namespace COTK_Utilities.Services.Config.Entities
{
    public class LocalePackPaths : ILocale
    {
        /// <summary>
        /// Краткая строковое обозначение локали языка. Пример = US
        /// </summary>
        [JsonProperty("Locale")]
        public string Locale { get; set; } = string.Empty;

        /// <summary>
        /// Абсолютный путь к каталогу содержащему файлы формата *.json, содержащие ПЕРЕВЕДЕННЫЕ ключи для страниц
        /// </summary>
        [JsonProperty("PagesDir")]
        [DefaultValue("")]
        public string PagesDir { get; set; } = string.Empty;

        /// <summary>
        /// Абсолютный путь к файлу формата *.json, содержащий общие ПЕРЕВЕДЕННЫЕ ключи для всех страниц
        /// </summary>
        [JsonProperty("CommonFilePath")]
        [DefaultValue("")]
        public string CommonFilePath { get; set; } = string.Empty;

        /// <summary>
        /// Абсолютный путь к каталогу содержащему файлы формата *.json, в которых хранятся готовые переводы ключей
        /// </summary>
        [JsonProperty("TranslatedKeyStoreDir")]
        public string TranslatedKeyStoreDir { get; set; }
    }
}
