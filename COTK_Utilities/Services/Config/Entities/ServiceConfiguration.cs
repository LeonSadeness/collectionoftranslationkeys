﻿using Newtonsoft.Json;
using System.ComponentModel;

namespace COTK_Utilities.Services.Config.Entities
{
    public class ServiceConfiguration
    {
        [JsonProperty("RootDir")]
        [DefaultValue("")]
        public string RootDir { get; set; } = "";

        [JsonProperty("Parser")]
        public ParserConfiguration ParserConfig { get; set; } = new ParserConfiguration();

        [JsonProperty("Updater")]
        public UpdaterConfiguration UpdaterConfig { get; set; } = new UpdaterConfiguration();
    }
}
