﻿using Newtonsoft.Json;

namespace COTK_Utilities.Services.Config.Entities
{
    [JsonObject("Updater")]
    public class UpdaterConfiguration
    {
        [JsonProperty("UpdateDirs")]
        public LocalePackPaths[] UpdateDirs { get; set; } = new LocalePackPaths[0];
    }
}
