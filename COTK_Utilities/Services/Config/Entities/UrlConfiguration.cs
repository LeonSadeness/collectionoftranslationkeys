﻿using Newtonsoft.Json;
using System.ComponentModel;

namespace COTK_Utilities.Services.Config.Entities
{
    [JsonObject]
    public class UrlConfiguration
    {
        [JsonProperty("Paths")]
        [DefaultValue(new string[0])]
        public string[] Paths { get; set; } = new string[0];

        [JsonProperty("ExcludePaths")]
        [DefaultValue(new string[0])]
        public string[] ExcludePaths { get; set; } = new string[0];
    }
}
