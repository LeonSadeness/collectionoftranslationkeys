﻿using Newtonsoft.Json;
using System.ComponentModel;

namespace COTK_Utilities.Services.Config.Entities
{
    [JsonObject]
    public class RegExpConfiguration
    {
        [JsonProperty("Keys")]
        [DefaultValue("")]
        public string Keys { get; set; } = string.Empty;
    }
}
