﻿using Newtonsoft.Json;

namespace COTK_Utilities.Services.Config.Entities
{
    [JsonObject("Parser")]
    public class ParserConfiguration
    {
        [JsonProperty("Pages")]
        public UrlConfiguration PagesUrl { get; set; } = new UrlConfiguration();

        [JsonProperty("Common")]
        public UrlConfiguration CommonUrl { get; set; } = new UrlConfiguration();

        [JsonProperty("RegExp")]
        public RegExpConfiguration RegexPatterns { get; set; } = new RegExpConfiguration();

        [JsonProperty("EmptyKeyGenerationDir")]
        public string EmptyKeyGenerationDir { get; set; } = string.Empty;
    }
}
