﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using COTK_Utilities.Entities;
using COTK_Utilities.Services.Config.Entities;
using COTK_Utilities.Services.Extensions;
using COTK_Utilities.Interfaces;

namespace COTK_Utilities.Services.Parser.Entities
{
    public class ParseResult
    {
        /// <summary>
        /// Словарь общих ключей
        /// </summary>
        public Wordbook CommonKeys { get; set; } = new Wordbook()
        {
            Name = "commonKeys",
            Tags = new Tag[1]
            {
                new Tag { Name = "Common", Description = "Общие ключи для всех страниц" }
            }

        };

        /// <summary>
        /// Коллекция ключей для страниц
        /// </summary>
        public BlockingCollection<Wordbook> PagesKeys { get; set; } = new BlockingCollection<Wordbook>();

        /// <summary>
        /// Объединенная коллекция всех коллекций ключей
        /// </summary>
        public IEnumerable<IWordbook> AllCollections => new List<Wordbook>(PagesKeys).Prepend(CommonKeys).ToList();

        /// <summary>
        /// Прогресс записи результата на диск
        /// </summary>
        public event Action<float> WriteProgressEvent;

        /// <summary>
        /// Запись результата на диск
        /// </summary>
        /// <param name="config"></param>
        public void WriteResult(ServiceConfiguration config)
        {
            var currentCount = 0;
            var totalCount = AllCollections.Aggregate(0, (a, c) => a + c.Dict.Count);
            if (totalCount == 0) return;
            var collectionDir = IOExtensions.GetDirInfo(config.ParserConfig.EmptyKeyGenerationDir, config.RootDir).FullName;

            foreach (var item in AllCollections)
            {
                using StreamWriter sw = new StreamWriter(Path.Join(collectionDir, item.Name + ".json"), false);
                var o = new JObject();
                foreach (var pair in item.Dict.OrderBy(i => i.Key))
                {
                    o[pair.Key] = "";
                    currentCount++;
                    WriteProgressEvent?.Invoke((float)currentCount / (float)totalCount);
                }
                sw.Write(o.ToString());
            }
        }
    }
}
