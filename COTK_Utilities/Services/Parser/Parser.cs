﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using COTK_Utilities.Services.Config.Entities;
using COTK_Utilities.Services.Parser.Entities;
using COTK_Utilities.Services.Extensions;
using COTK_Utilities.Interfaces;
using COTK_Utilities.Entities;

namespace COTK_Utilities.Services.Parser
{
    public class Parser
    {
        /// <summary>
        /// t("key")
        /// </summary>
        private readonly Regex _defaultRegexKeys = new Regex(@"[ ]*[^\w]t\([ \n\r]*[\'\""](?<key>.*?)[\'\""][ \n\r]*\)",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);
        public Regex RegexKeys => string.IsNullOrWhiteSpace(Config.ParserConfig.RegexPatterns.Keys)
            ? _defaultRegexKeys
            : new Regex(Config.ParserConfig.RegexPatterns.Keys, RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private readonly ParseResult _result = new ParseResult();
        public ParseResult Result => _result;

        private int _totalFiles;
        public int _currentProgress;
        public float Progress => _totalFiles > 0 ? (float)_currentProgress / (float)_totalFiles : 0;

        private bool _isProcessing;
        public bool IsProcessing => _isProcessing;

        public event Action<float> ProgressEvent;

        public readonly ServiceConfiguration Config;

        #region Constructor

        public Parser(ServiceConfiguration config)
        {
            Config = config;
        }
        #endregion

        public void Start()
        {
            if (!IsProcessing)
            {
                _isProcessing = true;

                var commonFiles = GetPathAndNameFiles(Config.ParserConfig.CommonUrl, Config.RootDir, Result.CommonKeys.Tags);
                var pageFiles = GetPathAndNameFiles(Config.ParserConfig.PagesUrl, Config.RootDir);

                IEnumerable<IFileInfo> files = pageFiles.Concat(commonFiles);

                _totalFiles = files.Count();
                _currentProgress = 0;
                ProgressEvent?.Invoke(Progress);

                _ = Parallel.ForEach(files, ParseFileAndSaveResult);

                // Post Clear Pages
                foreach (var coll in _result.PagesKeys)
                {
                    var filteredPage = coll.Dict.Where(i => _result.CommonKeys.Dict.All(j => j.Key != i.Key)).OrderBy(i => i.Key);
                    coll.Dict = new ConcurrentDictionary<string, string>(filteredPage);
                }

                _isProcessing = false;
            }
        }

        #region Files

        public IEnumerable<IFileInfo> GetPathAndNameFiles(UrlConfiguration urlConfig, string rootDir = null, ITag[] tags = null)
        {
            var options = new EnumerationOptions() { RecurseSubdirectories = true, };
            var pattern = "*";
            var excludes = urlConfig.ExcludePaths.Select(i => IOExtensions.GetDirInfo(i, rootDir));

            BlockingCollection<IFileInfo> result = new BlockingCollection<IFileInfo>();

            void AddFilesFromDir(string path)
            {
                var di = IOExtensions.GetDirInfo(path, rootDir);

                var files = Directory.GetFiles(di.FullName, pattern, options);
                var filtered = excludes.Count() > 0 ? files.Where(i => !excludes.Any(j => i.StartsWith(j.FullName))) : files;
                var converted = filtered.Select(i =>
                {
                    var relativePath = Path.GetRelativePath(di.FullName, i);
                    var endTrimCount = Path.GetExtension(relativePath).Length;
                    var name = relativePath.Substring(0, relativePath.Length - endTrimCount).Replace(Path.DirectorySeparatorChar, '.');
                    return new FileInformation { Name = name, Path = i, Tags = tags };
                });

                foreach (var item in converted) result.Add(item);
            }

            _ = Parallel.ForEach(urlConfig.Paths, AddFilesFromDir);

            return result;
        }
        #endregion

        #region Parsing

        public static BlockingCollection<string> ParseText(string text, Regex regex)
        {
            MatchCollection matches = regex.Matches(text);

            BlockingCollection<string> result = new BlockingCollection<string>();

            foreach (Match match in matches)
            {
                GroupCollection groups = match.Groups;
                string key = groups["key"].Value;
                if (result.Contains(key)) continue;
                result.Add(key);
            }

            return result;
        }

        void ParseFileAndSaveResult(IFileInfo fileInfo)
        {
            var text = File.ReadAllText(fileInfo.Path);
            var parsedKeys = ParseText(text, RegexKeys);

            if (fileInfo.ContainTag("Common"))
            {
                foreach (var item in parsedKeys) 
                    _result.CommonKeys.Dict[item] = "";
            }
            else
            {
                _result.PagesKeys.Add(new Wordbook
                {
                    Path = fileInfo.Path,
                    Name = fileInfo.Name,
                    Tags = fileInfo.Tags,
                    Dict = new ConcurrentDictionary<string, string>(parsedKeys.ToDictionary(k => k, v => "")),
                });
            }

            AddProgress();
        }

        static readonly object _lockToken = new object();
        void AddProgress()
        {
            lock (_lockToken)
            {
                _currentProgress++;
                ProgressEvent?.Invoke(Progress);
            }
        }
        #endregion
    }
}
