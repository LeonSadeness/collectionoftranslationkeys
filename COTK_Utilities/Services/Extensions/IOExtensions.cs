﻿using System.IO;
using COTK_Utilities.Services.Config.Entities;

namespace COTK_Utilities.Services.Extensions
{
    static class IOExtensions
    {
        #region Common

        public static DirectoryInfo GetDirInfo(string path, string rootDir = null)
        {
            var result = string.IsNullOrEmpty(rootDir)
                ? new DirectoryInfo(path)
                : new DirectoryInfo(Path.Join(rootDir, path));

            return result;
        }
        #endregion

        #region LocalePackPaths

        public static bool ExistFiles(this LocalePackPaths pack, string rootPath = null)
        {
            bool result = !string.IsNullOrWhiteSpace(pack.CommonFilePath)
                && File.Exists(rootPath == null
                ? pack.CommonFilePath
                : Path.Join(rootPath, pack.CommonFilePath));

            result &= !string.IsNullOrWhiteSpace(pack.PagesDir)
               && Directory.GetFiles(rootPath == null
                ? pack.PagesDir
                : Path.Join(rootPath, pack.PagesDir), "*.json").Length > 0;

            return result;
        }
        #endregion
    }
}
