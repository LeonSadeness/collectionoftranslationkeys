﻿namespace COTK_Utilities.Interfaces
{
    /// <summary>
    /// Тэг
    /// </summary>
    public interface ITag
    {
        /// <summary>
        /// Имя тэга
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание тэга
        /// </summary>
        public string Description { get; set; }
    }
}
