﻿using System.Collections.Generic;

namespace COTK_Utilities.Interfaces
{
    /// <summary>
    /// Именованная коллекция ключей файла
    /// </summary>
    public interface IWordbook : IFileInfo
    {
        /// <summary>
        /// Коллекция ключей
        /// </summary>
        IDictionary<string, string> Dict { get; set; }
    }
}