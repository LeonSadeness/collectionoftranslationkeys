﻿using System.Collections.Generic;

namespace COTK_Utilities.Interfaces
{
    public interface ILocalePackStore : IDirInfo, ILocale
    {
        /// <summary>
        /// Коллекция файлов хранящих переведенные ключи
        /// </summary>
        IWordbook[] StoreCollections { get; set; }

        /// <summary>
        /// Получить все ключи со всех хранилищ обьединенных в словаре
        /// </summary>
        /// <returns></returns>
        IDictionary<string, string> GetAllCollections();
    }
}
