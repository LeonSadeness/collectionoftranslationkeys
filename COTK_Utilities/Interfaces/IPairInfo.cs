﻿namespace COTK_Utilities.Interfaces
{
    public interface IPairInfo : ITagged, IModified
    {
        /// <summary>
        /// Ключ пары
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Значение пары
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Текст информации
        /// </summary>
        public string Description { get; set; }
    }
}