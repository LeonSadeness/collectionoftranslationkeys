﻿namespace COTK_Utilities.Interfaces
{
    /// <summary>
    /// Информация о файле
    /// </summary>
    public interface IFileInfo : ITagged
    {
        /// <summary>
        /// Абсолютный путь к файлу
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Имя файла без расширения
        /// </summary>
        public string Name { get; set; }
    }
}
