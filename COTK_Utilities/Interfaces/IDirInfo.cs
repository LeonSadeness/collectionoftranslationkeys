﻿namespace COTK_Utilities.Interfaces
{
    public interface IDirInfo : ITagged
    {
        /// <summary>
        /// Абсолютный путь к дирректории
        /// </summary>
        public string Dir { get; set; }
    }
}
