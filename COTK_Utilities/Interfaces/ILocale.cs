﻿namespace COTK_Utilities.Interfaces
{
    /// <summary>
    /// Локаль языка
    /// </summary>
    public interface ILocale
    {
        /// <summary>
        /// Краткое строковое обозначение локали языка. Пример = US
        /// </summary>
        public string Locale { get; set; }
    }
}
