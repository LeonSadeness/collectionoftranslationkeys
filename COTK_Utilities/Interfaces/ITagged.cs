﻿namespace COTK_Utilities.Interfaces
{
    public interface ITagged
    {
        /// <summary>
        /// Коллекция тегов
        /// </summary>
        public ITag[] Tags { get; set; }

        /// <summary>
        /// Проверяет наличие тэга
        /// </summary>
        /// <param name="tagName">Имя тэга</param>
        /// <returns>Булево значение наличие тэга</returns>
        public bool ContainTag(string tagName);

        /// <summary>
        /// Устанавливает тег, и возвращает true если он не был установлен ранее, и false если такой тэг уже был
        /// </summary>
        /// <param name="name">Имя тэга</param>
        /// <param name="description">Описание тэга</param>
        /// <returns>Наличие такого тэга</returns>
        bool SetTag(string name, string description = null);
    }
}
