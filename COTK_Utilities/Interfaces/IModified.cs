﻿using COTK_Utilities.Enums;

namespace COTK_Utilities.Interfaces
{
    public interface IModified
    {
        /// <summary>
        /// Тип изменения
        /// </summary>
        public ModificationType Modification { get; set; }
    }
}
