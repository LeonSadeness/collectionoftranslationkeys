using NUnit.Framework;

namespace CollectionOfTranslationKeys.Tests
{
    public class UtilitiesTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            string expected = "A B";
            string actual = string.Format("A {0}", "B");

            Assert.AreEqual(expected, actual);
        }
    }
}