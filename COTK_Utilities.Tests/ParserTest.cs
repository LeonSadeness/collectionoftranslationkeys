using NUnit.Framework;
using System.Collections.Concurrent;
using System.IO;
using COTK_Utilities.Services.Config;
using COTK_Utilities.Services.Config.Entities;
using COTK_Utilities.Services.Parser;

namespace COTK_Utilities.Tests
{
    public class ParserTest
    {
        private Configuration _config;

        [SetUp]
        public void Setup()
        {
            Preset.ClearTestDir();
            Preset.CreateTestFiles(4, 5, 2);

            var rootDir = Path.Join(Preset.RootDir, Preset.TestSrcDirName);

            UrlConfiguration pagesUrl = new UrlConfiguration
            {
                Paths = new string[] { Preset.PagesPath },
                ExcludePaths = new string[] { }
            };

            UrlConfiguration commonUrl = new UrlConfiguration
            {
                Paths = new string[] { Preset.JsDirName },
                ExcludePaths = new string[] { Preset.PagesPath }
            };

            _config = new Configuration
            {
                ServiceConfig = new ServiceConfiguration
                {
                    RootDir = rootDir,
                    ParserConfig = new ParserConfiguration
                    {
                        CommonUrl = commonUrl,
                        PagesUrl = pagesUrl,
                        EmptyKeyGenerationDir = "lang\\test",
                    },
                    UpdaterConfig = new UpdaterConfiguration
                    {
                        UpdateDirs = new LocalePackPaths[]
                        {
                            new LocalePackPaths{
                                Locale = "RU",
                                CommonFilePath = "lang\\ru.json",
                                PagesDir = "lang\\ru",
                                TranslatedKeyStoreDir = "store\\ru"
                            }
                        }
                    }
                },
            };
        }

        [Test]
        public void ParseText01()
        {
            Parser parser = new Parser(_config.ServiceConfig);

            var text = "<NavLink class=\"block w-full text - left\" :href=\"route('login')\">< GearSVG class=\"w-3 mr-1 inline\" />{{isUserLogin? t(\n\"My account\"\r\n) : t(\"Sign in\")}} </ NavLink >";
            BlockingCollection<string> expected = new BlockingCollection<string>
            {
                "My account",
                "Sign in"
            };
            BlockingCollection<string> actual = Parser.ParseText(text, parser.RegexKeys);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ParseText02()
        {
            Parser parser = new Parser(_config.ServiceConfig);

            var text = ":points=\"[\nt('All Basic Functiona, lities'),\nt('Premium Posts (be always on the top!)'),\nthis.t('See more details about the potential professionals'),\n]\"";
            BlockingCollection<string> expected = new BlockingCollection<string>
            {
                "All Basic Functiona, lities",
                "Premium Posts (be always on the top!)",
                "See more details about the potential professionals"
            };
            BlockingCollection<string> actual = Parser.ParseText(text, parser.RegexKeys);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ParseText03()
        {
            Parser parser = new Parser(_config.ServiceConfig);

            var text = @"// t('plus VAT (${vat_percent}) ${var_amount}')
// t('(VAT Reverse Charged)')
// t('Error: unknown log')
// t('Hooray! You've hired ${name_executor}. As soon as you're done with the project you can either initiate payment yourself or wait till ${name_executor} requests the payment.')
// t('Hooray! You've landed this job. As soon as you're done you can either request payment yourself or wait till ${name_client} initiates the payment.')
// t('${name_executor} has already finished their work and requests to be paid ${amount} ${vat}. Total amount is ${total_amount} You can either approve or change this amount.')
// t('${name_executor} has changed the requested amount to ${amount} ${vat}. Total amount is ${total_amount}. You can either approve or change this amount.')
// t('${name_executor} has approved the requested amount of ${amount} ${vat}. Total amount is ${total_amount}. You can pay now by clicking the Pay button.')
// t('You have requested the approval of payment amount ${amount} ${vat}. Total amount is ${total_amount}. Waiting for confirmation from ${name_executor}')
// t('You have approved the requested amount of ${amount} ${vat}. Total amount is ${total_amount}. You can pay now by clicking the Pay button.')
// t('You have changed the requested amount to ${amount} ${vat}. Total amount is ${total_amount}. Waiting for confirmation from ${name_executor}')
// t('You have requested to be paid ${amount} ${vat}. Total amount is ${total_amount}. Waiting for confirmation from ${name_client}')
// t('You have changed the requested amount of ${amount} ${vat}. Total amount is ${total_amount}. Waiting for confirmation from ${name_client}')
// t('You have approved the requested amount of ${amount} ${vat}. Total amount is ${total_amount}. Await the payment.')
// t('${name_client} has requested the approval of your payment amount ${amount} ${vat}. Total amount is ${total_amount}. You can either approve or change this amount.')
// t('${name_client} has approved the requested amount of ${amount} ${vat}. Total amount is ${total_amount}. Await the payment.')
// t('${name_client} has changed the requested amount to ${amount} ${vat}. Total amount is ${total_amount}. You can either approve or change this amount.')
// t('Congrats! You've successfully transferred ${amount} ${vat} to ${name_executor} and closed this contract. Leave us some feedback about this professional for future references')
// t('Congrats! ${name_client} has successfully paid you ${amount} ${vat} and closed this contract. The pay will be available for cashout in 7 days. You can check your stripe balance and withdraw your money ${link_dashboard_payment}. 10xDeals service fee is 7,5%. The amount you'll receive after service fees is ${total_amount_subtract_percent}')";
            BlockingCollection<string> expected = new BlockingCollection<string>
            {
                "plus VAT (${vat_percent}) ${var_amount}",
                "(VAT Reverse Charged)",
                "Error: unknown log",
                "Hooray! You've hired ${name_executor}. As soon as you're done with the project you can either initiate payment yourself or wait till ${name_executor} requests the payment.",
                "Hooray! You've landed this job. As soon as you're done you can either request payment yourself or wait till ${name_client} initiates the payment.",
                "${name_executor} has already finished their work and requests to be paid ${amount} ${vat}. Total amount is ${total_amount} You can either approve or change this amount.",
                "${name_executor} has changed the requested amount to ${amount} ${vat}. Total amount is ${total_amount}. You can either approve or change this amount.",
                "${name_executor} has approved the requested amount of ${amount} ${vat}. Total amount is ${total_amount}. You can pay now by clicking the Pay button.",
                "You have requested the approval of payment amount ${amount} ${vat}. Total amount is ${total_amount}. Waiting for confirmation from ${name_executor}",
                "You have approved the requested amount of ${amount} ${vat}. Total amount is ${total_amount}. You can pay now by clicking the Pay button.",
                "You have changed the requested amount to ${amount} ${vat}. Total amount is ${total_amount}. Waiting for confirmation from ${name_executor}",
                "You have requested to be paid ${amount} ${vat}. Total amount is ${total_amount}. Waiting for confirmation from ${name_client}",
                "You have changed the requested amount of ${amount} ${vat}. Total amount is ${total_amount}. Waiting for confirmation from ${name_client}",
                "You have approved the requested amount of ${amount} ${vat}. Total amount is ${total_amount}. Await the payment.",
                "${name_client} has requested the approval of your payment amount ${amount} ${vat}. Total amount is ${total_amount}. You can either approve or change this amount.",
                "${name_client} has approved the requested amount of ${amount} ${vat}. Total amount is ${total_amount}. Await the payment.",
                "${name_client} has changed the requested amount to ${amount} ${vat}. Total amount is ${total_amount}. You can either approve or change this amount.",
                "Congrats! You've successfully transferred ${amount} ${vat} to ${name_executor} and closed this contract. Leave us some feedback about this professional for future references",
                "Congrats! ${name_client} has successfully paid you ${amount} ${vat} and closed this contract. The pay will be available for cashout in 7 days. You can check your stripe balance and withdraw your money ${link_dashboard_payment}. 10xDeals service fee is 7,5%. The amount you'll receive after service fees is ${total_amount_subtract_percent}",
            };
            BlockingCollection<string> actual = Parser.ParseText(text, parser.RegexKeys);

            Assert.AreEqual(expected, actual);
        }
    }
}