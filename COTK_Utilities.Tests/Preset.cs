﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;
using System.Threading.Tasks;

namespace COTK_Utilities.Tests
{
    public static class Preset
    {
        #region Consts
        public static string RootDir { get; } = Directory.GetCurrentDirectory();
        public static string TestSrcDirName { get; } = "testSrc";
        public static string LangDirName { get; } = "lang";
        public static string JsDirName { get; } = "js";
        public static string PagesDirName { get; } = "Pages";

        public static string PagesPath => $"{JsDirName}{Path.DirectorySeparatorChar}{PagesDirName}";
        #endregion

        #region IO

        public static void ClearTestDir()
        {
            var path = Path.Join(RootDir, TestSrcDirName);
            if (Directory.Exists(path))
                Directory.Delete(path, true);
        }

        public static async void CreateTestFiles(int keysCount, int jsCount, int pagesCount)
        {
            var keys = new List<string>(CreateRandomKeys((keysCount * jsCount) + (keysCount * pagesCount)));

            for (int i = 0; i < jsCount; i++)
            {
                var text = CreateTextToParse(keys.GetRange(i, keysCount));
                var fileName = keys[i] + ".vue";
                var path = Path.Join(RootDir, TestSrcDirName, JsDirName);
                await WriteTextAsync(text, path, fileName);
            }
            for (int i = 0; i < pagesCount; i++)
            {
                var text = CreateTextToParse(keys.GetRange(i, keysCount));
                var fileName = keys[i] + ".vue";
                var path = Path.Join(RootDir, TestSrcDirName, JsDirName, PagesDirName);
                await WriteTextAsync(text, path, fileName);
            }
        }

        #endregion

        #region Text

        static string headerHTML = @"<template>
  <AuthCardLayout>
    <template #title>";
        static string footerHTML = @"</template>
  </AuthCardLayout>
</template>";

        static string headerJS = @"<script>
import AuthCardLayout from ""@/Components/Layouts/AuthCardLayout"";
import SelectTabsTop from ""@/Components/UI/SelectTabsTop"";
import ButtonForm from ""@/Components/UI/ButtonForm"";
import LabelDef from ""@/Components/UI/Label"";
import Input from ""@/Components/UI/Input"";
import ScaleAnim from ""@/Components/UI/Animation/ScaleAnim"";

export default {
  components: {
    AuthCardLayout,
    SelectTabsTop,
    ButtonForm,
    LabelDef,
    Input,
    ScaleAnim,
  },
  props: [""roles""],
    computed: {";
        static string footerJS = @"};
  },
methods: {
    submit() {
      this.form.post(this.route(""complete-registration.store""));
    },
  },
};
</ script > ";

        static public string CreateTextToParse(string[] keys) => CreateTextToParse(keys.ToList());

        static public string CreateTextToParse(List<string> keys)
        {
            if (keys == null || keys?.Count == 0) throw new ArgumentException("Нет ключей", "keys");
            var isMoreThanOne = keys?.Count > 1;
            int startIndex = keys.Count;

            var stringBuilder = new StringBuilder();
            stringBuilder.Append(headerHTML);
            stringBuilder.Append(GetTextHtmlComponent(keys[0]));

            if (isMoreThanOne)
            {
                startIndex = (int)Math.Round(keys.Count / 2f);
                for (int i = 0; i < startIndex; i++)
                {
                    stringBuilder.Append(GetTextHtmlComponent(keys[i]));
                }
            }
            stringBuilder.Append(footerHTML);

            if (isMoreThanOne)
            {
                stringBuilder.Append(headerJS);
                for (int i = startIndex; i < keys.Count; i++)
                {
                    stringBuilder.Append(GetTextJSComponent(keys[i]));
                }
                stringBuilder.Append(footerJS);
            }

            return stringBuilder.ToString();
        }

        static public string GetTextHtmlComponent(string key)
        {
            string text = $@"        <div class=\""space - y - 2\"">
              < LabelDef >{{ {{ t(""{key}"") }} }}</ LabelDef >
        
                  < Input
            id = ""first_name""
            class=""w-full""
            :placeholder=""t('{key}')""
            v-model=""form.first_name""
            autofocus
            autocomplete = ""given-name""
          />
        </ div > ";

            return text;
        }

        static public string GetTextJSComponent(string key)
        {
            string text = $@"optionsRoles() {{
      if (this.roles && this.roles.length > 0)
        return this.roles.map((r) => ({{
          title: r === this.t(""{key}"") ? ""professional"" : r,
          value: r,
        }}));
      else return [];
    }},";

            return text;
        }

        #endregion

        #region Utilities

        static string[] CreateRandomKeys(int count)
        {
            List<string> result = new List<string>();

            while (result.Count < count)
            {
                var word = CreateRandomWord();
                if (result.Any(w => w == word)) continue;
                result.Add(word);
            }

            return result.ToArray();
        }

        static string CreateRandomWord()
        {
            Random rnd = new Random();
            string[] words = { "Bold", "Think", "Friend", "Pony", "Fall", "Easy", "good_idea" };
            int randomNumber = rnd.Next(2000, 3000);

            string randomString = $"{words[rnd.Next(0, words.Length)]}{randomNumber}";

            return randomString;
        }

        public static async Task WriteTextAsync(string text, string dir, string fileName)
        {
            Directory.CreateDirectory(dir);
            var fullPath = Path.Join(dir, fileName);
            using var sw = new StreamWriter(fullPath, false);
            await sw.WriteAsync(text);
            await Task.CompletedTask;
        }

        #endregion
    }
}
