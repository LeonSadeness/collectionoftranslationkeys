﻿using COTK_Utilities.Entities;
using COTK_Utilities.Enums;
using COTK_Utilities.Services.Config;
using COTK_Utilities.Services.Config.Entities;
using COTK_Utilities.Services.Parser;
using COTK_Utilities.Services.Parser.Entities;
using COTK_Utilities.Services.Updater;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace COTK_Utilities.Tests
{
    public class UpdateTests
    {
        private Configuration _config;
        private string _rootDir = Path.Join(Preset.RootDir, Preset.TestSrcDirName);

        private LocalePackPaths _packRU = new LocalePackPaths
        {
            Locale = "RU",
            CommonFilePath = "lang/ru.json",
            PagesDir = "lang/ru",
            TranslatedKeyStoreDir = "store/ru"
        };

        [SetUp]
        public void Setup()
        {
            //Preset.ClearTestDir();
            //Preset.CreateTestFiles(4, 5, 2);


            UrlConfiguration pagesUrl = new UrlConfiguration
            {
                Paths = new string[] { Preset.PagesPath },
                ExcludePaths = new string[] { }
            };

            UrlConfiguration commonUrl = new UrlConfiguration
            {
                Paths = new string[] { Preset.JsDirName },
                ExcludePaths = new string[] { Preset.PagesPath }
            };

            _config = new Configuration
            {
                ServiceConfig = new ServiceConfiguration
                {
                    RootDir = _rootDir,
                    ParserConfig = new ParserConfiguration
                    {
                        CommonUrl = commonUrl,
                        PagesUrl = pagesUrl,
                        EmptyKeyGenerationDir = "lang/test",
                    },
                    UpdaterConfig = new UpdaterConfiguration
                    {
                        UpdateDirs = new LocalePackPaths[] { _packRU }
                    }
                },
            };

            //string fileName = "TestName_01.vue";
            //string jsDir = Path.Join(Preset.RootDir, Preset.TestSrcDirName, Preset.JsDirName);
            //string langDir = Path.Join(Preset.RootDir, Preset.TestSrcDirName, Preset.LangDirName, "ru");

            //string[] keysCode = new string[] { "one", "two", "three" };
            //var textCode = Preset.CreateTextToParse(keysCode);
            //Preset.WriteTextAsync(textCode, jsDir, fileName).GetAwaiter().GetResult();

            //string[] keysAvailable = new string[] { "one", "two", "three", "four" };
            //var textAvailable = Preset.CreateTextToParse(keysAvailable);
            //Preset.WriteTextAsync(textAvailable, langDir, fileName).GetAwaiter().GetResult();

            //Parser parser = new Parser(_config.ServiceConfig);
            //parser.Start();
        }

        [Test]
        public void UpdateAllKeys_ExistDelKeyResult()
        {
            ParseResult parseResult = new ParseResult
            {
                CommonKeys = new Wordbook
                {
                    Dict = new Dictionary<string, string>
                    {
                        ["one"] = "",
                        ["two"] = "",
                        ["three"] = "",
                    }
                }
            };

            Preset.ClearTestDir();
            string langDir = Path.Join(Preset.RootDir, Preset.TestSrcDirName, Preset.LangDirName);
            string[] keysAvailable = new string[] { "one", "two", "three", "four" };
            var json = new JObject();
            foreach (var key in keysAvailable) json[key] = "";
            var textAvailable = json.ToString();
            Preset.WriteTextAsync(textAvailable, langDir, $"{_packRU.Locale.ToLower()}.json").GetAwaiter().GetResult();

            Updater updater = new Updater(_config.ServiceConfig);
            var updateResult = updater.UpdateAllKeys(parseResult);

            ModificationType expected = updateResult.Changes.First().WordbookList.First().AllDict.First(i => i.Key == "four").Modification;
            ModificationType actual = ModificationType.DELETED;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void UpdateAllKeys_ExistAddKeyResult()
        {
            ParseResult parseResult = new ParseResult
            {
                CommonKeys = new Wordbook
                {
                    Dict = new Dictionary<string, string>
                    {
                        ["one"] = "",
                        ["two"] = "",
                        ["three"] = "",
                    }
                }
            };

            Preset.ClearTestDir();
            string langDir = Path.Join(Preset.RootDir, Preset.TestSrcDirName, Preset.LangDirName);
            string[] keysAvailable = new string[] { "one", "two" };
            var json = new JObject();
            foreach (var key in keysAvailable) json[key] = "";
            var textAvailable = json.ToString();
            Preset.WriteTextAsync(textAvailable, langDir, $"{_packRU.Locale.ToLower()}.json").GetAwaiter().GetResult();

            Updater updater = new Updater(_config.ServiceConfig);
            var updateResult = updater.UpdateAllKeys(parseResult);

            ModificationType expected = updateResult.Changes.First().WordbookList.First().AllDict.First(i => i.Key == "three").Modification;
            ModificationType actual = ModificationType.ADDED;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void UpdateKeysPack_ExistDelKeyResult()
        {
            ParseResult parseResult = new ParseResult
            {
                CommonKeys = new Wordbook
                {
                    Dict = new Dictionary<string, string>
                    {
                        ["one"] = "",
                        ["two"] = "",
                        ["three"] = "",
                    }
                }
            };

            Preset.ClearTestDir();
            string langDir = Path.Join(Preset.RootDir, Preset.TestSrcDirName, Preset.LangDirName);
            string[] keysAvailable = new string[] { "one", "two", "three", "four" };
            var json = new JObject();
            foreach (var key in keysAvailable) json[key] = "";
            var textAvailable = json.ToString();
            Preset.WriteTextAsync(textAvailable, langDir, $"{_packRU.Locale.ToLower()}.json").GetAwaiter().GetResult();

            Updater updater = new Updater(_config.ServiceConfig);
            var updateResult = updater.UpdateKeysPack(_packRU, parseResult);

            ModificationType expected = updateResult.WordbookList.First().AllDict.First(i => i.Key == "four").Modification;
            ModificationType actual = ModificationType.DELETED;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void UpdateKeysPack_ExistAddKeyResult()
        {
            ParseResult parseResult = new ParseResult
            {
                CommonKeys = new Wordbook
                {
                    Dict = new Dictionary<string, string>
                    {
                        ["one"] = "",
                        ["two"] = "",
                        ["three"] = "",
                    }
                }
            };

            Preset.ClearTestDir();
            string langDir = Path.Join(Preset.RootDir, Preset.TestSrcDirName, Preset.LangDirName);
            string[] keysAvailable = new string[] { "one", "two" };
            var json = new JObject();
            foreach (var key in keysAvailable) json[key] = "";
            var textAvailable = json.ToString();
            Preset.WriteTextAsync(textAvailable, langDir, $"{_packRU.Locale.ToLower()}.json").GetAwaiter().GetResult();

            Updater updater = new Updater(_config.ServiceConfig);
            var updateResult = updater.UpdateKeysPack(_packRU, parseResult);

            ModificationType expected = updateResult.WordbookList.First().AllDict.First(i => i.Key == "three").Modification;
            ModificationType actual = ModificationType.ADDED;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void UpdateKeysColl_ExistDelKeyResult()
        {
            var keysParse = new Dictionary<string, string>
            {
                ["one"] = "",
                ["two"] = "",
                ["three"] = "",
            };
            var keysAvailable = new Dictionary<string, string>
            {
                ["one"] = "",
                ["two"] = "",
                ["three"] = "",
                ["four"] = "",
            };

            Updater updater = new Updater(_config.ServiceConfig);
            var updateResult = updater.UpdateKeysWordbook(keysAvailable, keysParse);

            ModificationType expected = updateResult.AllDict.First(i => i.Key == "four").Modification;
            ModificationType actual = ModificationType.DELETED;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void UpdateKeysColl_ExistAddKeyResult()
        {
            var keysParse = new Dictionary<string, string>
            {
                ["one"] = "",
                ["two"] = "",
                ["three"] = "",
            };
            var keysAvailable = new Dictionary<string, string>
            {
                ["one"] = "",
                ["two"] = ""
            };

            Updater updater = new Updater(_config.ServiceConfig);
            var updateResult = updater.UpdateKeysWordbook(keysAvailable, keysParse);

            ModificationType expected = updateResult.AllDict.First(i => i.Key == "three").Modification;
            ModificationType actual = ModificationType.ADDED;

            Assert.AreEqual(expected, actual);
        }
    }
}
